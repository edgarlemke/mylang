#!/usr/bin/python3

import os

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
CONFIG = {
        # No need to add dependencies, it will be sorted out by kpm
        "input_files": [
            "src/kc.k",
        ],
        "script_dir": SCRIPT_DIR,
        "compiler_dir": "/home/gotcha/mylang/",
        "output_dir": "./build",
        "debug": False
}
