import copy
import argparse

import os
import sys
dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(dir_path)
import eval
from shared import debug


types_sizes = {
        "i1": 1,
        "i8": 8,
        "i16": 16,
        "i32": 32,
        "i64": 64,
        "i128": 128,
        "float": 32,
        "double": 64,
        "x86_fp80": 80,
        "fp128": 128,
        "ppc_fp128": 128
}


def __fn__(node, scope):
    debug(f"__fn__():  backend - node: {node}")

    global function_global_stack

    # compile-time validation
    import frontend.compiletime as compiletime
    compiletime.__fn__(node, scope)

    # back-end validation
    _validate_fn(node, scope)

    if len(node) == 4:
        fn_, name, arguments, body = node
    elif len(node) == 5:
        fn_, name, arguments, return_type, body = node

    # split_arguments = compiletime.split_function_arguments(arguments)

    debug(f"__fn__():  backend - arguments: {arguments}")

    # solve function overloaded name to a single function name
    uname = _unoverload(name, arguments)

    # get argument types
    args = []
    # convert argument types
    for arg in arguments:
        debug(f"__fn__(): arg in arguments: {arg}")

        if len(arg) == 2:
            converted_argument_type = _convert_type(arg[1], scope)

        elif len(arg) == 3 and arg[1] == "mut":
            converted_argument_type = _convert_type(arg[2], scope)

        args.append(f"{converted_argument_type} %{arg[0]}")

    # convert return type
    if len(node) == 4:
        return_type = "void"
    if len(node) == 5:

        is_composite = isinstance(return_type, list)

        if is_composite:
            main_type = return_type[0]
            main_type_name_value = eval.get_name_value(main_type, scope)

            if main_type_name_value != []:
                # check for generic variants
                if main_type_name_value[2] == "variant":
                    joined_types = "_".join(return_type)
                    return_type = f"%generic_variant_{joined_types}"

        else:
            return_type_name_value = eval.get_name_value(return_type, scope)
            debug(f"__fn__():  backend return_type_name_value: {return_type_name_value}")

            if return_type_name_value != []:
                if return_type_name_value[2] == "variant":
                    return_type = f"%{return_type}"
                else:
                    return_type = _convert_type(return_type, scope)

    debug(f"__fn__():  backend return_type: {return_type}")

    # create function body scope
    import copy
    function_body_scope = copy.deepcopy(eval.default_scope)

    # set scope child
    scope["children"].append(function_body_scope)

    # set function body scope parent
    function_body_scope["parent"] = scope

    # set scope return calls
    function_body_scope["return_call"] = scope["return_call"]
    function_body_scope["return_call_common_list"] = scope["return_call_common_list"]
    function_body_scope["return_call_common_not_list"] = scope["return_call_common_not_list"]
    function_body_scope["return_variant"] = scope["return_variant"]

    # set scope as backend scope
    function_body_scope["step"] = "backend"

    # setup function arguments
    for function_argument in arguments:
        debug(f"__fn__():  backend - function_argument: {function_argument}")

        if len(function_argument) == 2:
            mutdecl = "const"
            argument_type = function_argument[1]

        elif len(function_argument) == 3 and function_argument[1] == "mut":
            mutdecl = "mut"
            argument_type = function_argument[2]

        argument_list = [function_argument[0], mutdecl, argument_type, None]
        function_body_scope["names"].append(argument_list)

    # debug(f"__fn__():  backend - function_body_scope: {function_body_scope}")

    functions_stack.append([uname, function_body_scope, arguments])

    # sort out body IR
    function_body_scope["function_depth"] += 1
    result = eval.eval(body, function_body_scope)
    function_body_scope["function_depth"] -= 1

    functions_stack.pop()

    # if len(result) == 0:
    body = ["\tstart:"]
    # body = ["br label %start", "start:"]
    # else:

    if len(result) > 0:
        body.append(result)

    debug(f"__fn__():  backend - body: {body}")

    serialized_body = _serialize_body(body)

    debug(f"__fn__():  backend - serialized_body: {serialized_body}")

    # TODO: add check for last not empty string instead of last string that might be empty
    if "ret" not in serialized_body[len(serialized_body) - 1] and return_type == "void":
        # serialized_body.append([f"\t\tret {return_type}"])
        serialized_body.append([f"\t\tret void"])

    # declaration, definition = _write_fn(uname, args, return_type, body
    function_global = "\n".join(function_global_stack)
    debug(f"__fn__():  backend - function_global: {function_global}")

    retv = _write_fn(uname, args, return_type, serialized_body)

    if len(function_global) > 0:
        retv.insert(0, function_global)

    debug(f"__fn__():  backend - retv: {retv}")

    function_global_stack = []

    return retv


def _validate_fn(node, scope):
    import frontend.compiletime as compiletime

    # fn, args, ret_type, body = node
    if len(node) == 4:
        fn_, name, args, body = node
    elif len(node) == 5:
        fn_, name, args, ret_type, body = node

    debug(f"_validate_fn():  backend - name: {name} args: {args} body: {body}")

    # types = [t[0] for t in scope["names"] if t[2] == "type"]

    # check if types of the arguments are valid
    # split_args = compiletime.split_function_arguments(args)
    # debug(f"_validate_fn():  split_args: {split_args}")

    for arg in args:
        debug(f"_validate_fn():  backend - arg: {arg} args: {args}")

        if len(arg) == 2:
            name, type_ = arg
            mutdecl = "const"

        elif len(arg) == 3 and arg[1] == "mut":
            name, mutdecl, type_ = arg

        debug(f"_validate_fn():  backend - name: {name} type_: {type_}")

        # check for composite type
        if isinstance(type_, list):

            if type_[0] == "mut":
                search_type = type_[1]

            else:
                search_type = type_[0]

        # check for simple type
        else:
            search_type = type_

        # name_value = eval.get_name_value(type_, scope)
        # debug(f"_validate_fn():  backend - name_value: {name_value}")
        debug(f"_validate_fn():  backend - search_type: {search_type}")

        if not _validate_type(search_type, scope):  # name_value == [] or (name_value != [] and name_value[2] != "type"):
            raise Exception(f"Function argument has invalid type: {type_} {node}")

    # check if the return type is valid
    if len(node) == 5:  # and ret_type not in types:

        if not _validate_type(ret_type, scope):  # name_value == [] or (name_value != [] and name_value[2] != "type"):
            raise Exception(f"Function return type has invalid type: {ret_type} {node}")


functions_stack = []
function_global_stack = []


def __def__(node, scope):
    global function_global_stack

    debug(f"__def__():  backend - node: {node}")

    # compile-time validation
    import frontend.compiletime as ct
    ct.__def__(node, scope)

    # debug(f"\nscope: {scope}\n\n")

    # back-end validation
    _validate_def(node, scope)

    names = scope["names"]
    def_, mutdecl, name, data = node

    debug(f"__def__():  data: {data}")

    name_candidate = []
    if len(data) == 1:
        if isinstance(name, list):
            name_to_get = name[0]
        else:
            name_to_get = name

        name_candidate = eval.get_name_value(name_to_get, scope)
        if name_candidate == []:
            raise Exception(f"Unassigned name: {name}")

        type_ = name_candidate[2]

    elif len(data) == 2:
        type_ = data[0]

    debug(f"__def__():  type_: {type_}")

    tmp_name = None

    if isinstance(type_, list):
        if type_[0] == "Array":
            pass
        elif type_[0] == "ptr":
            pass

    elif len(functions_stack) > 0:  # and mutdecl == "mut":
        function_name = functions_stack[len(functions_stack) - 1][0]
        if len(data) == 1:
            _increment_ir_name_counter(function_name, name)

        tmp_name = _get_ir_name(function_name, name)
        _increment_ir_name_counter(function_name, name)

    else:
        # print(f"YAY: {name}")
        pass

    debug(f"__def__():  tmp_name: {tmp_name}")

    # check if name value is a struct reference
    if data[1][0] == "ref_member":
        debug(f"__def__():  is struct member reference")
        retv = __get_struct_member__(node, scope)

    elif data[1][0] == "ref_array_member":
        debug(f"__def__():  is array member reference")
        retv = __get_array_member__(node, scope)

    else:

        if type_ == "Str":
            str_, size = _converted_str(data[1])

            if mutdecl == "const":
                function_name = functions_stack[len(functions_stack) - 1][0]
                function_global_stack.append(f"""@{function_name}_{name} = constant [{size} x i8] c"{str_}" """)
                retv = f"""
\t\t%{tmp_name} = alloca %struct.Str, align 8
"""

            elif mutdecl == "mut":
                retv = f"""
\t\t%{tmp_name} = alloca %struct.Str, align 8

\t\t%{name}_str = alloca [{size} x i8]
\t\tstore [{size} x i8] c"{str_}", i8* %{name}_str

\t\t%{name}_str_ptr = getelementptr [{size} x i8], [{size} x i8]* %{name}_str, i64 0, i64 0

\t\t%{name}_addr_ptr = getelementptr %struct.Str, %struct.Str* %{tmp_name}, i32 0, i32 0
\t\tstore i8* %{name}_str_ptr, i8* %{name}_addr_ptr, align 8

\t\t%{name}_size_ptr = getelementptr %struct.Str, %struct.Str* %{tmp_name}, i32 0, i32 1
\t\tstore i64 {size}, i64* %{name}_size_ptr, align 8
""".split("\n")

        elif type_ in ["int", "uint", "float", "bool", "byte"]:
            debug("__def__():  backend - type_ is int uint float or bool or byte")

            t = _convert_type(type_, scope)

            if len(data) == 1:
                value = data[0]
                # _increment_ir_name_counter(function_name, name)
                # tmp_name = _get_ir_name(function_name, name)

            elif len(data) == 2:
                value = data[1]

            if type_ in ["int", "uint", "byte"]:
                if value[:2] == "0x":
                    value = int(value, base=16)

            debug(f"__def__():  backend - value: {value}")

            if isinstance(value, list):
                debug(f"__def__():  backend - calling return_call()")
                value, stack = return_call(value, scope, [])
                debug(f"__def__():  backend - value: {value} stack: {stack}")

                if mutdecl == "const":
                    stack.append(f"\t\t%{name} = {value}")

                elif mutdecl == "mut":
                    stack.append(f"\t\t%{tmp_name} = {value}")

                retv = "\n".join(stack)
                debug(f"__def__():  backend - retv: {retv}")

                # clean stack from return_call
                stack = []

            else:
                # check if it's at global backend scope
                if scope["parent"] is None:
                    debug(F"__def__():  backend - global scope")
                    retv = f"@{name} = global {t} {value}"

                # not global backend scope
                else:
                    debug(f"__def__():  backend - not global scope")

                # handle constants
                if mutdecl == "const":

                    if len(functions_stack) == 0:
                        retv = f"@{name} = constant {t} {value}"

                    else:
                        debug(f"functions_stack: {functions_stack}")

                        fn_name = functions_stack[0][0]

                        function_global_stack.append(f"@{fn_name}_{name} = constant {t} {value}")

                        # if is an integer read the pointer
                        if type_ in ["int", "uint", "byte"]:
                            retv = f"""\t\t; load value of constant "{name}"
\t\t%{name} = load {t}, {t}* @{fn_name}_{name}
"""

                        else:
                            retv = ""

                # handle mutable values
                elif mutdecl == "mut":
                    # allocate space in stack and set value
                    retv = f"""\t\t%{name}_stack = alloca {t}
\t\tstore {t} {value}, {t}* %{name}_stack
\t\t%{tmp_name} = load {t}, {t}* %{name}_stack
"""
            debug(f"__def__():  backend - retv: {retv}")

        elif type_ == "struct":
            #        if len(data) == 1:
            #            value = data[0]
            #            retv = "; STRUCT DEF"
            #
            #        elif len(data) == 2:
            if len(data) == 2:
                value = data[1]

                debug(f"__def__():  struct - value: {value}")

                # check for generic types definition
                if len(value[0]) == 1 and isinstance(value[0][0], list):
                    # retv is empty because actual structs are generated when references are evaluated
                    retv = ""
                else:
                    converted_members = []
                    for member in value:
                        debug(f"__def__():  struct - member: {member}")
                        converted_member_type = _convert_type(member[1], scope)
                        debug(f"__def__():  struct - converted_member_type: {converted_member_type}")
                        converted_members.append(converted_member_type)
                    joined_members = ", ".join(converted_members)

                    retv = f"""; "{name}" struct definition
%{name} = type {{{joined_members}}}"""

            else:
                raise Exception("Invalid size of struct")

        # handle variants
        elif type_ == "variant":
            if len(data) == 2:
                value = data[1]
                debug(f"__def__():  variant - value: {value}")

                # hangle generic variants definition
                if isinstance(value[0][0], list):
                    # do nothing because generic variants are generated afterwards
                    retv = ""  # _def_generic_variant(node, scope)

                # handle not-generic variant definition
                else:
                    # add default tag integer
                    converted_members = ["i8"]
                    member_types_variant_definitions = []

                    largest_member_type_size = 0

                    # iter over every member of the variant
                    for member in value:
                        debug(f"__def__():  variant - member: {member}")

                        total_member_types_size = 0

                        member_type_size = None
                        # iter over the types of member
                        for member_type in member[1:]:
                            debug(f"__def__():  variant - member_type: {member_type}")

                            # convert member type
                            converted_member_type = _convert_type(member_type, scope)
                            debug(f"__def__():  variant - converted_member_type: {converted_member_type}")

                            # get size of member type
                            member_type_size = 0
                            if converted_member_type[-1] == "*":
                                member_type_size = 8  # bytes
                            else:
                                member_type_size = types_sizes[converted_member_type]

                            if member_type_size == 0:
                                raise Exception("Couldn't get member type size")

                            debug(f"__def__():  variant - member_type_size: {member_type_size}")

                            total_member_types_size += member_type_size

                        # check if size of largest member type size isn't set
                        if largest_member_type_size == 0:
                            # if it isn't set, set it
                            largest_member_type_size = total_member_types_size
                        # else, check if current member type size is larger than largest member type size
                        elif total_member_types_size > largest_member_type_size:
                            # if it's larger, set largest member type size with larger value
                            largest_member_type_size = total_member_types_size

                        # append member type definition
                        type_variant_definition = ", ".join(converted_members + [f"i{member_type_size}"])
                        member_types_variant_definitions.append(f"%{name}_{member[0]} = type {{{type_variant_definition}}}")

                    converted_members.append(f"i{largest_member_type_size}")

                    joined_members = ", ".join(converted_members)
                    retv = f"""; "{name}" variant definition
%{name} = type {{{joined_members}}}
""" + "\n".join(member_types_variant_definitions) + "\n"

                    debug(f"__def__():  variant - retv: {retv}")

            else:
                raise Exception("Invalid size of variant")

        # test for composite types
        elif isinstance(type_, list):
            debug(f"__def__():  backend - type_: {type_}")

            type_value = eval.get_name_value(type_[0], scope)
            debug(f"__def__():  backend - type_value: {type_value}")

            # check for arrays
            # print(type_value)
            if type_value[0] == "Array":
                debug(f"__def__():  backend - Array found! data: {data} name_candidate: {name_candidate}")

                # check if array is already def
                retv = None
                if name_candidate != []:
                    member_index = 0
                    array_size = 64

                    retv = _set_array_member(type_value, member_index, type_, data[0], name[0], array_size)

                else:
                    retv = _def_array(type_value, type_, data, name)

            # check for pointers
            elif type_value[0] == "ptr":
                debug(f"__def__():  backend - ptr found! data: {data} name_candidate: {name_candidate}")

                retv = "RETV_PTR"

            # check for generic structs
            elif type_value[2] == "struct" and isinstance(type_value[3][0][0][0], list):
                debug(f"__def__():  backend - handling generic struct")

                # TODO: check if struct definition has already been defined
                #
                struct_definition_already_defined = False

                if not struct_definition_already_defined:
                    # generate struct definition
                    type_variables = type_value[3][0][0][0]
                    debug(f"__def__():  backend - generic struct - type_variables: {type_variables}")

                    type_values = data[0][1:]
                    debug(f"__def__():  backend - generic struct - type_values: {type_values}")

                    generated_struct_name, converted_type_values = _generate_generic_name(type_, type_values, scope, "generic_struct_")
                    name_pieces = [type_[0]] + type_values

                    import list as list_
                    list_print_struct_name = list_.list_print(name_pieces)

                    global_template = f"""; "{list_print_struct_name}" generic struct definition
%{generated_struct_name} = type {{{", ".join(converted_type_values)}}}
"""
                    function_global_stack.append(global_template)

                # instantiate struct definition in retv
#                retv = f"""\t\t; here goes struct use
# \t\t; %{name} = alloca %{generated_struct_name}
# """

                retv = _def_struct_init(type_, node, scope, generated_struct_name)

            # check for generic variants
            elif type_value[2] == "variant" and isinstance(type_value[3][0][0][0], list):
                debug(f"__def__():  backend - handling generic variant init")

                value = data[1]
                debug(f"__def__():  backend - variant - value: {value}")

                type_values = data[0][1:]
                debug(f"__def__():  backend - generic variant - type_values: {type_values}")

                variant_tags = [i[0] for i in type_value[3][0][1:]]
                debug(f"__def__():  backend - variant_tags: {variant_tags}")

                generated_variant_name, converted_type_values = _generate_generic_name(type_, type_values, scope, "generic_variant_")

                variant_definition_already_defined = _variant_already_defined("_".join(type_))
                if not variant_definition_already_defined:
                    _define_variant("_".join(type_))

                    # generate variant definition
                    # type_variables = type_value[3][0][0][0]
                    # debug(f"__def__():  backend - generic variant - type_variables: {type_variables}")
                    name_pieces = [type_[0]] + type_values

                    debug(f"__def__():  backend - generic variant - converted_type_values: {converted_type_values}")

                    converted_members = ["i8"]

                    # add default tag integer
                    member_types_variant_definitions = []

                    largest_member_type_size = 0
                    for cvt_index, member in enumerate(converted_type_values):
                        debug(f"__def__():  variant - member: {member}")

                        # get size of member type
                        member_type_size = 0
                        if member[-1] == "*":
                            member_type_size = 8  # bytes
                        else:
                            member_type_size = types_sizes[member]

                        if member_type_size == 0:
                            raise Exception("Couldn't get member type size")

                        debug(f"__def__():  variant - member_type_size: {member_type_size}")

                        # check if size of largest member type size isn't set
                        if largest_member_type_size == 0:
                            # if it isn't set, set it
                            largest_member_type_size = member_type_size
                        # else, check if current member type size is larger than largest member type size
                        elif member_type_size > largest_member_type_size:
                            # if it's larger, set largest member type size with larger value
                            largest_member_type_size = member_type_size

                        # append member type definition
                        type_variant_definition = ", ".join(converted_members + [f"i{member_type_size}"])
                        member_types_variant_definitions.append(f"%{generated_variant_name}_{variant_tags[cvt_index]} = type {{{type_variant_definition}}}")

                    converted_members.append(f"i{largest_member_type_size}")
                    # joined_members = ", ".join(converted_members)

                    debug(f"__def__():  backend - generic variant - converted_members: {converted_members}")
                    debug(f"__def__():  backend - generic variant - member_types_variant_definitions: {member_types_variant_definitions}")

                    import list as list_
                    list_print_variant_name = list_.list_print(name_pieces)

                    global_template = f"""; "{list_print_variant_name}" generic variant definition
%{generated_variant_name} = type {{{", ".join(converted_members)}}}
""" + "\n".join(member_types_variant_definitions) + "\n;"

                    function_global_stack.append(global_template)

                retv = _def_variant(type_, node, scope, generated_variant_name)

            else:
                raise Exception(f"Unknown composite type {type_}")

        else:

            type_name_value = eval.get_name_value(type_, scope)
            debug(f"__def__():  backend - type_name_value: {type_name_value}")

            found_struct = type_name_value != [] and type_name_value[2] == "struct"
            found_variant = type_name_value != [] and type_name_value[2] == "variant"

            # check for structs
            if found_struct:
                debug(f"__def__():  backend - found_struct - type_name_value: {type_name_value} data: {data}")
                retv = _def_struct_init(type_, node, scope)

            # check for variants
            elif found_variant:
                debug(f"__def__():  backend - found_variant - type_name_value: {type_name_value} data: {data}")
                retv = _def_variant(type_, node, scope)

            else:
                raise Exception(f"Unknown type: {type_}")

    debug(f"__def__():  backend - exiting __def__()")

    return retv


def _def_array(type_value, type_, data, name):
    unset = False
    end = len(type_)
    if end == 2:
        array_size = len(data[1])

    elif end == 3:
        end -= 1
        array_size = int(type_[2])
        unset = True

    array_struct_name = type_value[0]  # "_".join([type_value[0]] + type_[1:end])
    array_members_type = _convert_type(type_[1])

    debug(f"_def_array():  end: {end} array_size: {array_size} array_struct_name: {array_struct_name} array_members_type: {array_members_type} data: {data}")

    if not unset and not isinstance(data[1], list) and type_[1] == "byte":
        converted_str = _converted_str(data[1])
        array_size = converted_str[1]

    # setup stack with start of array initialization
    stack = [f"""\t\t; start of initialization of "{name}" Array
\t\t; allocate stack for "{name}" Array
\t\t%{name} = alloca %{array_struct_name}

\t\t; allocate stack for Array members
\t\t%{name}_members = alloca [{array_size} x {array_members_type}]
"""]

    if not _already_declared(functions_stack[0][0], f"{name}_members_ptr"):
        _declare_name(functions_stack[0][0], f"{name}_members_ptr")
        stack.append(f"""\t\t; setup members pointer in Array
\t\t%{name}_members_ptr = getelementptr %{array_struct_name}, %{array_struct_name}* %{name}, i64 0
""")

    stack.append(f"""\t\tstore [{array_size} x {array_members_type}]* %{name}_members, [{array_size} x {array_members_type}]* %{name}_members_ptr
""")

    if not unset:
        # initialize array members

        # check for array of bytes
        if not isinstance(data[1], list) and type_[1] == "byte":
            value_to_store = f"c\"{converted_str[0]}\""

#            stack.append(f"""\t\t; member {member_index} initialization
# \t\t%{name}_member_{member_index}_ptr = getelementptr [{array_size} x {array_members_type}], [{array_size} x {array_members_type}]* %{name}_Array_members, i32 0, i32 {member_index}
# \t\tstore {array_members_type} {value_to_store}, {array_members_type}* %{name}_member_{member_index}_ptr
# """)

        else:

            array_ir_buffer = []
            for member_index in range(0, array_size):
                array_ir_buffer.append(f"{array_members_type} {data[1][member_index]}")

            value_to_store = f"""[{",".join(array_ir_buffer)}]"""

        stack.append(f"""\t\t; init stack for "{name}" Array members
\t\tstore [{array_size} x {array_members_type}] {value_to_store}, [{array_size} x {array_members_type}]* %{name}_members
""")

    # end of array initialization
    stack.append(f"""\t\t; setup "{name}" Array size as {array_size}
\t\t%{name}_size_ptr = getelementptr %{array_struct_name}, %{array_struct_name}* %{name}, i32 0, i32 1
\t\tstore i64 {array_size}, i64* %{name}_size_ptr
\t\t; end of initialization of "{name}" Array
""")

    retv = "\n".join(stack)
    return retv


def _set_array_member(type_value, member_index, type_, data, name, array_size):
    array_members_type = _convert_type(type_[1])

    value = data

    retv = f"""\t\t%{name}_member_{member_index}_ptr = getelementptr [{array_size} x {array_members_type}], [{array_size} x {array_members_type}]* %{name}, i32 0, i32 {member_index}
\t\tstore {array_members_type} %{value}, {array_members_type}* %{name}_member_{member_index}_ptr
"""

    return retv


def _def_struct_init(type_, node, scope, generated_struct_name=None):
    name = node[2]
    data = node[3]

    debug(f"_def_struct_init():  name: {name} type: {type_} data: {data} scope:{hex(id(scope))}")

    if isinstance(type_, list):
        is_generic_struct = True
        tmp_type = type_[0]
    else:
        is_generic_struct = False
        tmp_type = type_

    type_name_value = eval.get_name_value(tmp_type, scope)
    debug(f"_def_struct_init():  type_name_value: {type_name_value}")

    members_container = type_name_value[3][0]

    if is_generic_struct:
        type_variables_container = type_name_value[3][0][0]
        members_rest = type_name_value[3][0][1:]
        type_rest = type_[1:]

        debug(f"_def_struct_init():  type_variables_container: {type_variables_container}")
        debug(f"_def_struct_init():  members_rest: {members_rest}")
        debug(f"_def_struct_init():  type_rest: {type_rest}")

        # TODO: validate if they have the same size
        # new_members_container = []
        # for i_index, i_member in enumerate(members_rest):
        #    new_members_container.append([i_member[0], type_rest[i_index]])
        # debug(f"_def_struct_init():  new_members_container: {new_members_container}")
        # members_container = new_members_container

        debug(f"_def_struct_init():  type_name_value: {type_name_value} type_rest: {type_rest}")
        members_container = _substitute_struct_generic_types(type_name_value, type_rest)

        # fix type_ for IR
        type_ = generated_struct_name

    debug(f"_def_struct_init():  members_container: {members_container}")

    converted_members = []
    for member_index, member in enumerate(members_container):
        debug(f"_def_struct_init():  backend - struct member: {member}")

        member_value = data[1][member_index]
        debug(f"_def_struct_init():  backend - struct member_value: {member_value}")

        member_value_name_value = eval.get_name_value(member_value, scope)
        debug(f"_def_struct_init():  backend - member_value_name_value: {member_value_name_value}")

        if member_value_name_value != []:

            struct_candidate_name = member_value_name_value[2]
            struct_candidate_name_value = eval.get_name_value(struct_candidate_name, scope)
            if struct_candidate_name_value[2] == "struct":
                member_value = f"%{member_value}"

        converted_member_type = _convert_type(member[1], scope)

        # converted_members.append(converted_member_type)
        converted_members.append(f"""\t\t; setting "{name}.{member[0]}" to "{member_value}"
\t\t%{name}_{member[0]} = getelementptr %{type_}, %{type_}* %{name}, i32 0, i32 {member_index}
\t\tstore {converted_member_type} {member_value}, {converted_member_type}* %{name}_{member[0]}""")

    debug(f"_def_struct_init():  backend - converted_members: {converted_members}")

    converted_members_text = "\n".join(converted_members)

    retv = f"""\t\t; "{name}" of type struct "{type_}" definition
\t\t%{name} = alloca %{type_}

{converted_members_text}
"""

    return retv


def _def_generic_variant(node, scope):
    debug(f"_def_generic_variant():  node: {node} scope: {hex(id(scope))}")

    retv = ""
    return retv


def _generate_generic_name(type_, type_values, scope, prefix):
    name_pieces = [type_[0]] + type_values
    debug(f"_generate_generic_name():  backend - name_pieces: {name_pieces}")

    generated_name = "_".join(name_pieces)

    converted_type_values = []
    for type_value in type_values:
        converted_type_value = _convert_type(type_value, scope)
        converted_type_values.append(converted_type_value)

    debug(f"_generate_generic_name():  backend - converted_type_values: {converted_type_values}")

    generated_name = f"{prefix}{generated_name}"

    return (generated_name, converted_type_values)


def _converted_str(str_):
    encoded = str_.encode('utf-8')

    buf = []
    size = 0
    for ch in encoded:
        size += 1

        if ch < 128:
            buf.append(chr(ch))

        else:
            buf.append(f"\\{format(ch, 'x')}")

    result = "".join(buf)

    return (result, size)


def _validate_def(node, scope):
    debug(f"_validate_def():  backend - node: {node}")

    import eval

    def_, mutdecl, name, data = node

    if len(data) == 1:
        debug(f"_validate_def():  backend - len(data) == 1 - name: {name}")

        if isinstance(name, list):
            name_to_get = name[0]

        else:
            name_to_get = name

        name_candidate = eval.get_name_value(name_to_get, scope)
        if name_candidate == []:
            raise Exception(f"Reassigning not assigned name: {name} {data}")

        debug(f"_validate_def():  backend - name_candidate: {name_candidate}")

        type_ = name_candidate[2]
        debug(f"_validate_def():  backend - type_: {type_}")

    elif len(data) == 2:
        debug(f"_validate_def():  backend - len(data) == 2")

        type_ = data[0]
        # value = data[1]

        if isinstance(type_, list):
            debug(f"_vadidate_def():  backend - type_ is list")

            if len(type_) < 2 or len(type_) > 3:
                raise Exception("Constant assignment has invalid type - type_: {type_}")

            if type_[0] == "Array":
                debug(f"_validate_def():  backend - type is Array: {type_}")

                # validate generic type
                valid_member_type = _validate_members_type(type_[1], scope)
                if not valid_member_type:
                    raise Exception("Constant assignment has invalid type - type_[1]: {type_[1]}")

                debug(f"_validate_def():  backend - valid member type")

                # check for array size and undef
                if len(type_) == 3:
                    debug(f"_validate_def():  backend - len of type_ is 3")

                    array_size = type_[2]
                    try:
                        int(array_size)
                    except BaseException:
                        raise Exception("Constant assignment of Array with invalid size - array_size: {array_size}")

                    if data[1] != "unset":
                        raise Exception("Constant assignment of sized Array for value other than \"unset\" - value: {data[1]}")

                type_ = type_[0]

    # validate type
    if not _validate_type(type_, scope):
        raise Exception(f"Constant assignment has invalid type - type_: {type_} - node: {node}")

    debug(f"_validate_def():  backend - node OK: {node}")

    return


def _validate_members_type(members_type, scope):
    debug(f"_validate_members_type():  members_type: {members_type}")

    valid = True

    if not isinstance(members_type, list):
        return _validate_type(members_type, scope)

    def iter(type_):
        debug(f"iter():  type_: {type_}")

        for child in type_:
            if isinstance(child, list):
                iter(child)

    iter(members_types)

    return valid


def _def_variant(type_, node, scope, generated_variant_name=None):
    debug(f"_def_variant():  type: {type_} node: {node} scope: {hex(id(scope))} scope names: {scope['names']}")

    def_, mutdecl, name, data = node

    variant_name, variant_value = data
    debug(f"_def_variant():  variant_name: {variant_name} variant_value: {variant_value}")

    if isinstance(type_, list):
        is_generic_variant = True
        tmp_type = type_[0]
    else:
        is_generic_variant = False
        tmp_type = type_

    debug(f"_def_variant():  is_generic_variant: {is_generic_variant}")

    name_name_value = eval.get_name_value(name, scope)
    debug(f"_def_variant():  name_name_value: {name_name_value}")

    retv = []

    # TODO: add to validate
    if not isinstance(variant_value, list):
        raise Exception(f"Variant value isn't a list: {variant_value}")

    # TODO: fix fn call for generic variants

    # check for variant member
    is_variant_member = False

    type_name_value = eval.get_name_value(tmp_type, scope)
    debug(f"_def_variant():  type_name_value: {type_name_value}")

    if type_name_value == []:
        raise Exception(f"Variant value tag is invalid: {type_}")

    is_variant_member = type_name_value[2] == "variant"
    if is_variant_member:
        some_member_matches = False
        for member in type_name_value[3][0]:
            debug(f"_def_variant():  member: {member}")
            if member[0] == variant_value[0]:
                some_member_matches = True
                break
        is_variant_member = some_member_matches

    debug(f"_def_variant():  is_variant_member: {is_variant_member}")

    tag_type = variant_value[0]
    debug(f"_def_variant():  tag_type: {tag_type}")

    type_name_value = eval.get_name_value(tmp_type, scope)
    debug(f"_def_variant():  type_name_value: {type_name_value}")

    members_container = type_name_value[3][0]
    if is_generic_variant:
        type_variables_container = type_name_value[3][0][0]
        members_rest = type_name_value[3][0][1:]
        type_rest = type_[1:]

        debug(f"_def_variant():  type_variables_container: {type_variables_container}")
        debug(f"_def_variant():  members_rest: {members_rest}")
        debug(f"_def_variant():  type_rest: {type_rest}")

        # TODO: validate if they have the same size

        debug(f"_def_variant():  type_name_value: {type_name_value} type_rest: {type_rest}")
        members_container = _substitute_variant_generic_types(type_name_value, type_rest)

        debug(f"_def_variant():  members_container: {members_container}")

        # fix type_ for IR
        type_ = generated_variant_name
        variant_name = generated_variant_name

    function_name = functions_stack[0][0]
    tmp_name = _get_ir_name(function_name, f"{name}_ptr")
    _increment_ir_name_counter(function_name, f"{name}_ptr")

    retv = f"""\t\t; allocate space for variant B
\t\t%{tmp_name} = alloca %{type_}
"""

    # get tag value from tag type
    tag_value = ""

    # get tags of variant
    variant_name_name_value = eval.get_name_value(tmp_type, scope)
    # if variant_name_name_value == []:
    #    raise Exception("Blergh!")

    debug(f"_def_variant():  variant_name_name_value: {variant_name_name_value}")

    tags = variant_name_name_value[3][0]
    debug(f"_def_variant():  tags: {tags}")

    proper_tags = []
    idx_offset = 0
    if is_generic_variant:
        proper_tags = tags[1:]
    else:
        proper_tags = tags
        idx_offset = 1
    enumerated_tags = enumerate(proper_tags)

    for idx, each_tag in enumerated_tags:
        debug(f"_def_variant():  idx: {idx} each_tag: {each_tag} tag_type: {tag_type}")

        extracted_tag = each_tag[0]

        if extracted_tag == variant_value[0]:
            tag_value = idx
            break

    if tag_value == "":
        # check if "tag type" ;) is an internal or function call
        tag_type_name_value = eval.get_name_value(tag_type, scope)
        debug(f"_def_variant():  tag_type_name_value: {tag_type_name_value}")

        if tag_type_name_value != [] and tag_type_name_value[2] in ["internal", "fn"]:
            debug(f"_def_variant():  handling function call or internal! {variant_value}")

            variant_definition = _def_variant_fn_call(tag_type, node, scope)
            debug(f"_def_variant():  variant_definition: {variant_definition}")

            return variant_definition

        else:
            raise Exception("Couldn't find tag value!")

    debug(f"_def_variant():  tag_value: {tag_value}")
    #

    variant_tag_type = f"{variant_name}_{tag_type}"
    debug(f"_def_variant():  variant_tag_type: {variant_tag_type}")

    converted_values = []

    to_iter = None
    if not is_generic_variant:
        to_iter = enumerate(variant_value[idx_offset:])
    else:
        to_iter = enumerate(variant_value[idx_offset + 1:])

    for idx, each_variant_value in to_iter:
        debug(f"_def_variant():  idx: {idx} each_variant_value: {each_variant_value}")

        value_is_list = isinstance(each_variant_value, list)

        to_get_name_value = each_variant_value if not value_is_list else each_variant_value[0]
        variant_value_name_value = eval.get_name_value(to_get_name_value, scope)
        debug(f"_def_variant():  variant_value_name_value: {variant_value_name_value}")

        if variant_value_name_value != [] and variant_value_name_value[2] == "fn":
            debug(f"_def_variant():  each_variant_value is fn!")

            converted_tag_value = "%" + _get_ir_name(function_name, "variant_fn_call_result")

            method, solved_arguments = eval.find_function_method(each_variant_value, variant_value_name_value, scope)
            debug(f"_def_variant():  method: {method} solved_arguments: {solved_arguments}")

            function_return_type = method[1]
            converted_tag_type = _convert_type(function_return_type, scope)

            variant_function_name = each_variant_value[0]

            args = None
            if is_generic_variant:
                args = method[0]
            else:
                args = method[0][0]

            ir_function_name = _unoverload(variant_function_name, args)

            converted_arguments = []
            for arg in solved_arguments:
                converted_arguments.append(f"{_convert_type(arg[0], scope)} {arg[1]}")
            debug(f"_def_variant():  converted_arguments: {converted_arguments}")

            joined_arguments = ", ".join(converted_arguments)

            retv += f"""\t\t; get function call return value
\t\t{converted_tag_value} = call {converted_tag_type} @{ir_function_name}({joined_arguments})
\t\t;
"""

        else:
            converted_tag_type = ""
            converted_tag_value = ""
            if variant_value_name_value != []:
                converted_tag_type = _convert_type(variant_value_name_value[2], scope)

                if variant_value_name_value[1] == "mut":
                    converted_tag_value = f"%{_get_ir_name(function_name, each_variant_value, last= True)}"
                elif variant_value_name_value[1]:
                    converted_tag_value = f"%{each_variant_value}"

            else:
                debug(f"_def_variant():  proper_tags: {proper_tags}")

                if not is_generic_variant:
                    tag_type_to_convert = proper_tags[idx + idx_offset][1]
                    debug(f"_def_variant():  tag_type_to_convert: {tag_type_to_convert}")

                    converted_tag_type = _convert_type(tag_type_to_convert, scope)
                    converted_tag_value = variant_value[idx_offset]

                else:
                    tag_type_to_convert = proper_tags[idx + idx_offset][1]
                    debug(f"_def_variant():  tag_type_to_convert: {tag_type_to_convert}")

                    # TODO: get tag type
                    decl_type_variable_idx = None
                    for type_variable_idx, type_variable in enumerate(type_variables_container[0]):
                        debug(f"_def_variant():  type_variable_idx: {type_variable_idx}")

                        if type_variable == tag_type_to_convert:
                            decl_type_variable_idx = type_variable_idx
                            break

                    debug(f"_def_variant():  decl_type_variable_idx: {decl_type_variable_idx}")

                    if decl_type_variable_idx is None:
                        raise Exception(f"Couldn't match tag type with variant type variables: {tag_type_to_convert}")

                    decl_type = type_rest[decl_type_variable_idx]
                    debug(f"_def_variant():  decl_type: {decl_type}")

                    converted_tag_type = _convert_type(decl_type, scope)
                    converted_tag_value = variant_value[idx_offset + 1]

            debug(f"_def_variant():  converted_tag_type: {converted_tag_type}")
            debug(f"_def_variant():  converted_tag_value: {converted_tag_value}")

        converted_values.append([converted_tag_type, converted_tag_value])

    debug(f"_def_variant():  converted_values: {converted_values}")

    tmp_tag_ptr = _get_ir_name(function_name, f"{name}_tag_ptr")
    _increment_ir_name_counter(function_name, f"{name}_tag_ptr")

    tmp_name_casted_ptr = _get_ir_name(function_name, f"{name}_{tag_type}_casted_ptr")
    _increment_ir_name_counter(function_name, f"{name}_{tag_type}_casted_ptr")

    # TODO: make it default when creating names
    if len(name_name_value) == 4:
        name_name_value.append({})

    name_name_value[4]["bitcast_name"] = tmp_name_casted_ptr
    name_name_value[4]["type"] = variant_tag_type

    retv += f"""\t\t; set tag
\t\t%{tmp_tag_ptr} = getelementptr %{variant_name}, %{variant_name}* %{tmp_name}, i32 0, i32 0
\t\tstore i8 {tag_value}, i8* %{tmp_tag_ptr}
\t\t; bitcast
\t\t%{tmp_name_casted_ptr} = bitcast %{variant_name}* %{tmp_name} to %{variant_tag_type}*
\t\t;
"""

    if True:  # not is_generic_variant:
        for idx, converted_value in enumerate(converted_values):
            debug(f"_def_variant():  idx: {idx} converted_value: {converted_value}")

            tmp_name_value_ptr = _get_ir_name(function_name, f"{name}_value_ptr")
            _increment_ir_name_counter(function_name, f"{name}_value_ptr")

            converted_tag_type, converted_tag_value = converted_value

            retv += f"""\t\t; set member {idx} value
\t\t%{tmp_name_value_ptr} = getelementptr %{variant_tag_type}, %{variant_tag_type}* %{tmp_name_casted_ptr}, i32 0, i32 {idx+1}
\t\tstore {converted_tag_type} {converted_tag_value}, {converted_tag_type}* %{tmp_name_value_ptr}
\t\t;
"""
    else:

        pass
        # for idx, converted_value in enumerate(converted_values):
        #    debug(f"_def_variant():  idx: {idx} converted_value: {converted_value}")

    return retv


def _def_variant_fn_call(type_, node, scope):
    debug(f"_def_variant_fn_call():  type_: {type_} node: {node} scope: {hex(id(scope))}")

    def_, mutdecl, name, data = node

    function_name = functions_stack[0][0]
    tmp_name = _get_ir_name(function_name, f"{name}_ptr", last=True)
    # _increment_ir_name_counter(function_name, f"{name}_ptr")

    tmp_loaded = _get_ir_name(function_name, f"{name}")
    # _increment_ir_name_counter(function_name, f"{name}")

    variant_name, variant_value = data
    debug(f"_def_variant_fn_call():  variant_name: {variant_name} variant_value: {variant_value}")

    tmp_variant_name = None
    generic_variant_name = None

    is_generic = isinstance(variant_name, list)
    if is_generic:
        joined_name = "_".join(variant_name)
        generic_variant_name = f"generic_variant_{joined_name}"
        tmp_variant_name = generic_variant_name
    else:
        tmp_variant_name = variant_name

    variant_value_name_value = eval.get_name_value(variant_value[0], scope)
    debug(F"_def_variant_fn_call():  variant_value_name_value: {variant_value_name_value}")

    method, solved_arguments = eval.find_function_method(variant_value, variant_value_name_value, scope)
    debug(f"_def_variant_fn_call():  method: {method}")

    called_function_arguments = method[0]  # [0]
    debug(f"_def_variant_fn_call():  called_function_arguments: {called_function_arguments}")

    unoverloaded_fn_name = _unoverload(variant_value[0], called_function_arguments)
    debug(f"_def_variant_fn_call():  unoverloaded_fn_name: {unoverloaded_fn_name}")

    name_name_value = eval.get_name_value(name, scope)

    # get the return type
    return_type = method[1]
    tmp_return_type = None
    generic_return_type = None

    # check for generic variants in return type
    is_return_type_generic = isinstance(return_type, list)
    if is_return_type_generic:
        joined_types = "_".join(return_type)
        generic_return_type = f"generic_variant_{joined_types}"
        tmp_return_type = generic_return_type
    else:
        tmp_return_type = return_type

    retv = [f"""\t\t; allocate space for variant A
\t\t%{tmp_name} = alloca %{tmp_variant_name}"""]

    debug(f"_def_variant_fn_call():  tmp_return_type: {tmp_return_type} tmp_variant_name: {tmp_variant_name}")

    if tmp_return_type != tmp_variant_name:
        debug(f"_def_variant_fn_call():  tmp_return_type != tmp_variant_name - {tmp_return_type} != {tmp_variant_name}")

        gnv_name = None
        if is_generic:
            gnv_name = variant_name[0]
        else:
            gnv_name = variant_name

        debug(f"_def_variant_fn_call():  gnv_name: {gnv_name}")

        # get return type tag
        variant_name_name_value = eval.get_name_value(gnv_name, scope)
        debug(f"_def_variant_fn_call():  variant_name_name_value: {variant_name_name_value}")

        tags = None
        if not is_generic:
            tags = variant_name_name_value[3][0]
        else:
            retvalue = data[0]
            tags = [[x] for x in retvalue[1:]]

        debug(f"_def_variant_fn_call():  tags: {tags}")

        found_tag_index = None
        for tag_index, tag_list in enumerate(tags):
            tag = tag_list[0]
            if tag == return_type:
                found_tag_index = tag_index

        # TODO: proper validation before
        if found_tag_index is None:
            raise Exception("Invalid function return type for variant value")

        tag_value = found_tag_index

        # set tag
        tmp_tag_ptr = _get_ir_name(function_name, f"{name}_tag_ptr")
        _increment_ir_name_counter(function_name, f"{name}_tag_ptr")

        tag_type = return_type
        tmp_name_casted_ptr = _get_ir_name(function_name, f"{name}_{tag_type}_casted_ptr")
        _increment_ir_name_counter(function_name, f"{name}_{tag_type}_casted_ptr")

        variant_tag_type = f"{tmp_variant_name}_{tag_type}"

        # TODO: make it default when creating names
        if len(name_name_value) == 4:
            name_name_value.append({})

        name_name_value[4]["bitcast_name"] = tmp_name_casted_ptr
        name_name_value[4]["type"] = variant_tag_type

        retv.append(f"""\t\t; set tag
\t\t%{tmp_tag_ptr} = getelementptr %{tmp_variant_name}, %{tmp_variant_name}* %{tmp_name}, i32 0, i32 0
\t\tstore i8 {tag_value}, i8* %{tmp_tag_ptr}
\t\t; bitcast
\t\t%{tmp_name_casted_ptr} = bitcast %{tmp_variant_name}* %{tmp_name} to %{variant_tag_type}*""")

    # try to get bitcast name
    if len(name_name_value) == 5:
        tmp_name = name_name_value[4]["bitcast_name"]
        variant_name = name_name_value[4]["type"] = variant_tag_type

    debug(f"_def_variant_fn_call():  tmp_name after trying to get bitcast name: {tmp_name}")
    debug(f"_def_variant_fn_call():  variant_name after trying to get bitcast name: {variant_name}")
    debug(f"_def_variant_fn_call():  solved_arguments: {solved_arguments}")

    # get function call arguments
    converted_arguments = []
    for arg_i, fn_arg in enumerate(solved_arguments):
        debug(f"_def_variant_fn_call():  arg_i: {arg_i} fn_arg: {fn_arg}")

        converted_arg_type = _convert_type(fn_arg[0], scope)
        debug(f"_def_variant_fn_call():  converted_arg_type: {converted_arg_type}")

        converted_arguments.append(f"{converted_arg_type} {fn_arg[1]}")

    debug(f"_def_variant_fn_call():  converted_arguments: {converted_arguments}")

    converted_arguments_ir = ", ".join(converted_arguments)
    debug(f"_def_variant_fn_call():  converted_arguments_ir: {converted_arguments_ir}")

    retv.append(f"""\t\t; call function and get return value
\t\t%{tmp_loaded}  = call %{tmp_variant_name} @{unoverloaded_fn_name}({converted_arguments_ir})
\t\t; store return value in allocated space
\t\tstore %{tmp_variant_name} %{tmp_loaded}, %{tmp_variant_name}* %{tmp_name}
""")
    return "\n".join(retv)


def _unoverload(name, function_arguments):
    debug(f"_unoverload():  name: {name} function_arguments: {function_arguments} ")

    arg_types = []
    for arg in function_arguments:
        debug(f"_unoverload():  arg: {arg}")
        arg_types.append(arg[1])

    unamel = [name]
    if len(arg_types) > 0:

        buffer = []
        for arg_type in arg_types:
            debug(f"_unoverload():  arg_type: {arg_type}")

            if isinstance(arg_type, list):
                # buffer.append("_".join(arg_type))

                list_arg_type_buffer = []

                def _serialize(li):
                    for child in li:
                        if isinstance(child, list):
                            _serialize(child)
                        else:
                            list_arg_type_buffer.append(child)
                _serialize(arg_type)
                buffer.append("_".join(list_arg_type_buffer))

            else:
                buffer.append(arg_type)

        unamel += ["__", "_".join(buffer)]
    uname = "".join(unamel)
    # print(f"uname: {uname}")

    return uname


def _convert_type(type_, scope=None):
    debug(f"_convert_type():  type_: {type_}")

    converted_type = None

    # convert integers
    if type_ in ["int", "uint"]:
        converted_type = "i64"

    # convert bytes
    if type_ == "byte":
        converted_type = "i8"

    # convert booleans
    elif type_ == "bool":
        converted_type = "i1"

    # convert floats
    elif type_ == "float":
        converted_type = "float"

    # convert pointers
    elif type_[0] == "ptr":
        converted_type = f"{_convert_type(type_[1])}*"

    elif type_[0] == "Array":
        converted_type = f"i8*"

    # convert Str (strings)
    elif type_ == "Str":
        converted_type = "%struct.Str*"

    elif scope is not None:
        # convert structs
        type_name_value = eval.get_name_value(type_, scope)
        if type_name_value != [] and type_name_value[2] == "struct":
            converted_type = f"%{type_}*"

    # else:
    #    raise Exception(f"NYAA ${type_}")
    #    #converted_type = f"%{type_}*"

    debug(f"_convert_type():  converted_type: {converted_type}")

    return converted_type


def _validate_type(type_, scope):
    # get types and structs from scope and parent scopes
    all_types = []
    all_structs = []
    all_variants = []

    def iterup(scope, all_types, all_structs, all_variants):
        # print(f"\n!! iterup - scope: {scope}\n")

        parent_scope = scope["parent"]
        children_scopes = scope["children"]

        if parent_scope is not None:
            iterup(parent_scope, all_types, all_structs, all_variants)

        types = [t[0] for t in scope["names"] if t[2] == "type" and t[0] not in all_types]
        all_types += types
        # print(f"types: {types}")

        structs = [s[0] for s in scope["names"] if s[2] == "struct" and s[0] not in all_structs]
        all_structs += structs
        # print(f"structs: {structs}")

        variants = [s[0] for s in scope["names"] if s[2] == "variant" and s[0] not in all_variants]
        all_variants += variants

    iterup(scope, all_types, all_structs, all_variants)
    exceptions = ["fn"]
    valid_types = (all_types + all_structs + all_variants + exceptions)

    debug(f"_validate_type():  valid_types: {valid_types}")

    def loop(current_type):
        for each_type in current_type:
            debug(f"loop():  each_type: {each_type}")

            if isinstance(each_type, list):
                return loop(each_type)

            else:
                if each_type not in valid_types:
                    return False

        return True

    if isinstance(type_, list):
        return_value = loop(type_)
    else:
        return_value = loop([type_])

    return return_value


def _write_fn(fn, args, return_type, body):
    # arg_types = [ arg[1] for arg in args ]
    # decl_args = ", ".join(arg_types)
    # declaration = f"declare {return_type} @{fn}({decl_args})"
    # print(f"declaration: {declaration}")

    def_args = ", ".join(args)
    definition = [f"define {return_type} @{fn}({def_args}) {{", body, "}"]
    # print(f"definition: {definition}")

    # return [declaration, definition]
    return definition


def _serialize_body(body):
    debug(f"_serialize_body():  {body}")

    serialized = []

    def iter(li):
        debug(f"iter():  li: {li}")

        for child in li:
            debug(f"\niter():  child: {child}")
            if isinstance(child, list):
                debug(f"iter():  gonna iter over {child}")
                iter(child)
            else:
                debug(f"iter():  appending {child}")
                serialized.append(child)

        debug(f"\niter():  exiting {li}\n\n -> serialized: {serialized}\n")

    iter(body)

    debug(f"_serialize_body():  - exiting -> serialized: {serialized}\n")

    return serialized


def __set__(node, scope):
    debug(f"__set__():  backend - node: {node} scope: {hex(id(scope))}")

    import eval
    import list as list_

    _validate_set(node, scope)

    name = node[1]
    value = node[2]

    name_value = eval.get_name_value(name, scope)
    debug(f"__set__():  backend - name: {name} name_value: {name_value} value: {value} {type(value)}")

    type_ = _convert_type(name_value[2], scope)

    template = []

    if isinstance(value, list):
        debug(f"__set__():  backend - value is list, going to return_call() it - value: {value}")

        tmp_name = _get_ir_name(functions_stack[0][0], name)
        _increment_ir_name_counter(functions_stack[0][0], name)

        printed_list = list_.list_print(value)

        evaluated_value = return_call(value, scope)

        template.append(evaluated_value[1])
        template.append(f"""\t\t; set "{name}" value as result of "{printed_list}"
\t\t%{tmp_name} = {evaluated_value[0]}
\t\tstore {type_} %{tmp_name}, {type_}* %{name}_stack""")

    else:
        debug(f"__set__():  backend - value isn't list")

        # check if value is a name
        value_name_value = eval.get_name_value(value, scope)
        debug(f"__set__():  backend - value_name_value: {value_name_value}")
        if value_name_value != []:

            value_name_value_type = value_name_value[2]
            value_name_value_type_name_value = eval.get_name_value(value_name_value_type, scope)

            debug(f"__set__():  backend - value_name_value_type_name_value: {value_name_value_type_name_value}")

            if value_name_value_type_name_value[2] == "variant":
                variant_value_ptr_name = _get_ir_name(functions_stack[0][0], f"{value}_value_ptr")
                tmp_name = _get_ir_name(functions_stack[0][0], f"tmp")
                _increment_ir_name_counter(functions_stack[0][0], f"tmp")

                name_ir = _get_ir_name(functions_stack[0][0], f"{name}", track_branching=True)
                _increment_ir_name_counter(functions_stack[0][0], f"{name}")

                template.append(f"""\t\t; get value from "{value}" variant value ptr
\t\t%{tmp_name} = load {type_}, {type_}* %{variant_value_ptr_name}
\t\t; set "{name}" value obtained from "{value}"
\t\tstore {type_} %{tmp_name}, {type_}* %{name}_stack
\t\t; update "{name}" IR name
\t\t%{name_ir} = load {type_}, {type_}* %{name}_stack""")

            else:
                name_ir = _get_ir_name(functions_stack[0][0], f"{name}", track_branching=True)
                _increment_ir_name_counter(functions_stack[0][0], f"{name}")

                value = _get_ir_name(functions_stack[0][0], f"{value}", last=True)

                template.append(f"""\t\t; set "{name}" value as "{value}"
\t\tstore {type_} %{value}, {type_}* %{name}_stack
\t\t; update "{name}" IR name
\t\t%{name_ir} = load {type_}, {type_}* %{name}_stack""")

        # TODO: support for other types
        else:
            name_ir = _get_ir_name(functions_stack[0][0], f"{name}", track_branching=True)
            _increment_ir_name_counter(functions_stack[0][0], f"{name}")

            template.append(f"""\t\t; set "{name}" value as "{value}"
\t\tstore {type_} {value}, {type_}* %{name}_stack
\t\t; update "{name}" IR name
\t\t%{name_ir} = load {type_}, {type_}* %{name}_stack""")

    debug(f"__set__():  backend - template: {template}")

    return template


def _validate_set(node, scope):

    debug(f"_validate_set():  backend - node: {node}")

    # validate name mutability
    name = None
    if isinstance(node[1], list):
        name = node[1][0]

    else:
        name = node[1]

    name_value = eval.get_name_value(name, scope)
    debug(f"_validate_set():  backend - name_value: {name_value}")

    # useful for checking set inside functions that couldn't be checked at frontend phase
    if name_value[1] == "const":
        raise Exception(f"Resetting constant name: {node} {name_value}")

# ARRAY INTERNALS
#


def __set_array_member__(node, scope):
    _validate_set_array_member(node, scope)

    node, name, indexes, value = node

    name_value = eval.get_name_value(name, scope)

    type_ = name_value[2]
    converted_type = _convert_type(type_, scope)
    if converted_type[-1] == "*":
        converted_type = converted_type[0:-1]

    import list as list_

    # iter over indexes
    template = []
    index = None
    lp_reference = list_.list_print(indexes)

    debug(f"__set_array_member__():  backend - indexes: {indexes}")
    for i in indexes:
        debug(f"__set_array_member__():  backend - i: {i}")
        # test if they're int-able
        int_index = True
        try:
            int(i)
        except BaseException:
            int_index = False

        debug(f"__set_array_member__():  backend - int_index: {int_index}")

        # not int-able
        if int_index:
            index = i

        else:
            # try to solve variable name
            index_name_value = eval.get_name_value(i, scope)
            i_name, i_mutdecl, i_type, i_value = index_name_value

            index = f"%{i_name}"

            debug(f"__set_array_member__():  backend - index: {index} index_name_value: {index_name_value}")

        # add bound check code
        array_name = name
        array_index = i
        function_name = functions_stack[0][0]
        array_size_tmp_name = _get_ir_name(function_name, f"{array_name}_size")
        _increment_ir_name_counter(function_name, f"{array_name}_size")

        array_inbounds_tmp_name = _get_ir_name(function_name, f"{array_name}_inbounds")
        _increment_ir_name_counter(function_name, f"{array_name}_inbounds")

        template.append(f"""\t\t; check bound
\t\t; load array size
\t\t%{array_size_tmp_name} = load i64, i64* %{array_name}_size_ptr
\t\t; compare index and array size
\t\t%{array_inbounds_tmp_name} = icmp ult i64 {array_index}, %{array_size_tmp_name}
\t\t; branch
\t\tbr i1 %{array_inbounds_tmp_name}, label %{array_inbounds_tmp_name}_ok, label %{array_inbounds_tmp_name}_err
\t\t; error label
\t{array_inbounds_tmp_name}_err:
\t\t; TODO: write to stderr
\t\t; TODO: unwind stack
\t\t; exit
\t\tcall void @linux_exit(i64 -1)
\t\tunreachable
\t\t; ok label
\t{array_inbounds_tmp_name}_ok:
\t\t""")

        # index is set
        template.append(f"""\t\t; set "{name}" member "{i}"
""")

        if not _already_declared(functions_stack[0][0], f"{name}_members_ptr"):
            _declare_name(functions_stack[0][0], f"{name}_members_ptr")
            template.append(f"""\t\t; get members pointer
\t\t%{name}_members_ptr = getelementptr i8*, i8** %{name}
""")

        template.append(f"""\t\t%{name}_members_ptr_ = load i8*, i8* %{name}_members_ptr

\t\t; get specific member pointer
\t\t%{name}_member_ptr = getelementptr i8, i8** %{name}_members_ptr_, i64 {index}
""")

        # TODO: multidimensional arrays
        # break at first index
        break

    debug(f"__set_array_member__():  backend - index: {index}")

    infered_value = eval._infer_type(value)
    debug(f"__set_array_member__():  backend - value: {value} infered_value: {infered_value}")

    if infered_value is not None:
        converted_infered_type = _convert_type(infered_value[0], scope)
        debug(f"__set_array_member__():  backend - converted_infered_type: {converted_infered_type}")

        if infered_value[0] in ["int", "uint"] and value[:2] == "0x":
            mangled_value = int(value, base=16)
        else:
            mangled_value = value

        template.append(f"""\t\t; store "{value}" value at member pointer
\t\tstore {converted_infered_type} {mangled_value}, i8** %{name}_member_ptr
""")

    else:

        name_value_ = eval.get_name_value(value, scope)
        function_name = functions_stack[0][0]

        if name_value_[1] == "const":
            template.append(f"""\t\tstore {converted_type} %{name_value_[0]}, i8** %{name}_member_ptr
""")

        elif name_value[1] == "mut":
            tmp_name_value = _get_ir_name(function_name, value)
            _increment_ir_name_counter(function_name, value)

            template.append(f"""\t\t%{tmp_name_value} = load {converted_type}, {converted_type}* %{value}_stack
\t\t; store value at member pointer
\t\tstore {converted_type} %{tmp_name_value}, i8** %{name}_member_ptr
""")

    return template


def _validate_set_array_member(node, scope):
    import frontend.compiletime as ct
    ct.validate_set_array_member(node, scope)


def __get_array_member__(node, scope):
    debug(f"__get_array_member__():  node: {node} scope: {hex(id(scope))}")

    # _validate_get_array_member(node, scope)

    def_, mutdecl, name, data = node
    debug(f"__get_array_member__():  data: {data}")

    ref_array_member_, array_name, array_index = data[1]

    type_ = data[0]
    converted_type = _convert_type(type_, scope)
    if converted_type[-1] == "*":
        converted_type = converted_type[0:-1]

    function_name = functions_stack[0][0]
    # tmp_name = _get_ir_name(function_name, name)
    # _increment_ir_name_counter(function_name, name)

    array_size_tmp_name = _get_ir_name(function_name, f"{array_name}_size")
    _increment_ir_name_counter(function_name, f"{array_name}_size")

    array_inbounds_tmp_name = _get_ir_name(function_name, f"{array_name}_inbounds")
    _increment_ir_name_counter(function_name, f"{array_name}_inbounds")

    template = f"""\t\t; check array index bound
\t\t; load array size
\t\t%{array_size_tmp_name} = load i64, i64* %{array_name}_size_ptr
\t\t; compare array size and index
\t\t%{array_inbounds_tmp_name} = icmp ult i64 {array_index}, %{array_size_tmp_name}
\t\t; branch
\t\tbr i1 %{array_inbounds_tmp_name}, label %{array_inbounds_tmp_name}_ok, label %{array_inbounds_tmp_name}_err
\t{array_inbounds_tmp_name}_err:
\t\t; TODO: write to stderr
\t\t; TODO: unwind stack
\t\t; exit
\t\tcall void @linux_exit(i64 -1)
\t\tunreachable
\t{array_inbounds_tmp_name}_ok:
\t\t; get array member
\t\t%{array_name}_{array_index}_ptr = getelementptr {converted_type}, {converted_type}* %{array_name}_members, i32 {array_index}
\t\t%{name} = load {converted_type}, {converted_type}* %{array_name}_{array_index}_ptr
"""

    return template


def _validate_get_array_member(node, scope):
    import frontend.compiletime as ct
    ct.validate_get_array_member(node, scope)
#
#

# STRUCT INTERNALS
#


def __set_struct_member__(node, scope):
    _validate_set_struct_member(node, scope)

    set_struct_member_, struct_name, struct_member, value = node

    struct_name_value = eval.get_name_value(struct_name, scope)
    debug(f"__set_struct_member__():  struct_name_value: {struct_name_value}")

    structdef_name = struct_name_value[2]
    structdef_name_value = eval.get_name_value(struct_name_value[2], scope)

    members = structdef_name_value[3]
    debug(f"__set_struct_member__():  members: {members}")

    member_type = ""
    for member_index, member in enumerate(members):
        debug(f"__set_struct_member__():  member: {member}")

        if member[1] == struct_member:
            member_type = _convert_type(member[0], scope)
            break

    stack = []

    if not _already_declared(functions_stack[0][0], f"{struct_name}_{struct_member}_member_ptr"):
        _declare_name(functions_stack[0][0], f"{struct_name}_{struct_member}_member_ptr")
        stack.append(f"""\t\t; get struct "{structdef_name}" "{struct_name}" member "{struct_member}" pointer
\t\t%{struct_name}_{struct_member}_member_ptr = getelementptr {member_type}, %{structdef_name}* %{struct_name}, i64 {member_index}
""")

    stack.append(f"""\t\t; set struct "{structdef_name}" "{struct_name}" member "{struct_member}"
\t\tstore {member_type} {value}, {member_type}* %{struct_name}_{struct_member}_member_ptr
""")

    return "\n".join(stack)


def _validate_set_struct_member(node, scope):
    import frontend.compiletime as ct
    ct.validate_set_struct_member(node, scope)


def __get_struct_member__(node, scope):
    #    _validate_get_struct_member(node, scope)

    debug(f"__get_struct_member__():  node: {node}")

    def_, mutdecl, name, data = node
    debug(f"__get_struct_member__():  data: {data}")

    stack = []

    def loop(data, lvl):
        debug(f"__get_struct_member__():  loop():  data: {data}")

        child_is_ref_member = isinstance(data[1], list) and data[1][0] == "ref_member"
        debug(f"__get_struct_member__():  loop():  child_is_ref_member: {child_is_ref_member}")

        ref_member_, struct_name, struct_member = data

        if child_is_ref_member:
            struct_name = loop(data[1], lvl + 1)
            debug(f"__get_struct_member__():  loop():  new struct_name: {struct_name}")

        debug(f"__get_struct_member__():  loop():  struct_name: {struct_name} struct_member: {struct_member} data: {data}")

        struct_name_value = eval.get_name_value(struct_name, scope)
        debug(f"__get_struct_member__():  loop():  struct_name_value: {struct_name_value}")

        structdef_name = struct_name_value[2]
        ir_structdef_name = structdef_name
        if isinstance(structdef_name, list):
            structdef_name = struct_name_value[2][0]

        structdef_name_value = eval.get_name_value(structdef_name, scope)
        debug(f"__get_struct_member__():  loop():  structdef_name_value: {structdef_name_value}")

        members = structdef_name_value[3][0]
        debug(f"__get_struct_member__():  loop():  members: {members}")

        # mangle members for generic structs
        is_generic_struct = isinstance(members[0][0], list)
        debug(f"__get_struct_member__():  loop():  is_generic_struct: {is_generic_struct}")

        if is_generic_struct:
            type_values = struct_name_value[2][1:]
            debug(f"__get_struct_member__():  loop():  type_values: {type_values}")

            members = _substitute_struct_generic_types(structdef_name_value, type_values)
            structdef_name, converted_types = _generate_generic_name(struct_name_value[2], type_values, scope, "generic_struct_")

        # get member type
        member_type = ""
        for member_index, member in enumerate(members):
            debug(f"__get_struct_member__():  loop():  member: {member}")
            if member[0] == struct_member:
                member_type = _convert_type(member[1], scope)
                break

        if not _already_declared(functions_stack[0][0], f"{struct_name}_{struct_member}_member_ptr"):
            _declare_name(functions_stack[0][0], f"{struct_name}_{struct_member}_member_ptr")
            stack.append(f"""\t\t%{struct_name}_{struct_member}_member_ptr = getelementptr {member_type}, %{structdef_name}* %{struct_name}, i64 {member_index}
""")

        debug(f"__get_struct_member__():  loop():  new stack: {stack}")

        if lvl > 0:
            return struct_name_value[3][member_index]

        else:
            return [member_type, struct_name, struct_member]

    x = loop(data[1], 0)
    member_type, struct_name, struct_member = x
    debug(f"__get_struct_member__():  x: {x}")

    debug(f"__get_struct_member__():  stack: {stack}")

#    get_struct_member_, struct_name, struct_member = data[1]
#
#    debug(f"__get_struct_member__():  struct_name: {struct_name} struct_member: {struct_member}")
#
#    struct_name_value = eval.get_name_value(struct_name, scope)
#    debug(f"__get_struct_member__():  struct_name_value: {struct_name_value}")
#
#    structdef_name = struct_name_value[2]
#    structdef_name_value = eval.get_name_value(struct_name_value[2], scope)
#
#    members = structdef_name_value[3][0]
#    debug(f"__get_struct_member__():  members: {members}")
#
#    member_type = ""
#    for member_index, member in enumerate(members):
#        debug(f"__get_struct_member__():  member: {member}")
#
#        if member[0] == struct_member:
#            member_type = _convert_type(member[1])
#            break
#
#    stack = []
#
#    if not _already_declared(functions_stack[0][0], f"{struct_name}_{struct_member}_member_ptr"):
#        _declare_name(functions_stack[0][0], f"{struct_name}_{struct_member}_member_ptr")
#        stack.append(f"""\t\t%{struct_name}_{struct_member}_member_ptr = getelementptr {member_type}, %{structdef_name}* %{struct_name}, i64 {member_index}""")

    _declare_name(functions_stack[0][0], f"{name}")
    stack.append(f"""\t\t; get struct member
\t\t%{name} = load {member_type}, {member_type}* %{struct_name}_{struct_member}_member_ptr
""")

    return "\n".join(stack)


def _substitute_struct_generic_types(structdef_name_value, type_values):
    debug(f"_substitute_struct_generic_types():  structdef_name_value: {structdef_name_value}  type_values: {type_values}")

    members = structdef_name_value[3][0]
    debug(f"_substitute_struct_generic_types():  members: {members}")

    type_variables = members[0][0]
    debug(f"_substitute_struct_generic_types():  type_variables: {type_variables}")

    # type_values = type_name_value[2][1:]
    # debug(f"_substitute_struct_generic_types():  type_values: {type_values}")

    substituted_types = {}
    for tv_index, tv in enumerate(type_variables):
        substituted_types[tv] = type_values[tv_index]
    debug(f"_substitute_struct_generic_types():  substituted_types: {substituted_types}")

    new_members = []
    for member in members[1:]:
        debug(f"_substitute_struct_generic_types():  member: {member}")
        type_variable = member[1]

        if type_variable in substituted_types.keys():
            member[1] = substituted_types[type_variable]
        new_members.append(member)

    debug(f"_substitute_struct_generic_types():  new_members: {new_members}")

    return new_members


def _substitute_variant_generic_types(variantdef_name_value, type_values):
    debug(f"_substitute_variant_generic_types():  variantdef_name_value: {variantdef_name_value}  type_values: {type_values}")

    members = variantdef_name_value[3][0]
    debug(f"_substitute_variant_generic_types():  members: {members}")

    type_variables = members[0][0]
    debug(f"_substitute_variant_generic_types():  type_variables: {type_variables}")

    # type_values = type_name_value[2][1:]
    # debug(f"_substitute_variant_generic_types():  type_values: {type_values}")

    substituted_types = {}
    for tv_index, tv in enumerate(type_variables):
        substituted_types[tv] = type_values[tv_index]
    debug(f"_substitute_variant_generic_types():  substituted_types: {substituted_types}")

    new_members = []
    for member in members[1:]:
        debug(f"_substitute_variant_generic_types():  member: {member}")
        type_variable = member[0]

        if type_variable in substituted_types.keys():
            member[0] = substituted_types[type_variable]
        new_members.append(member)

    debug(f"_substitute_variant_generic_types():  new_members: {new_members}")

    return new_members


# def _validate_get_struct_member(node, scope):
#    # Removed because backend must handle struct member access differently
#    #import frontend.compiletime as ct
#    #ct.validate_get_struct_member(node, scope)
#
#    ref_member_node = node[3][1]

#
#


def __ref_member__(node, scope):
    debug(f"__ref_member__():  node: {node} scope: {hex(id(scope))}")

    return ["yaya"]

#    return __get_struct_member__(node, scope)

#    # TODO: add validation for members existence
#
#    # get struct
#    ref_member, struct_name, struct_member = node
#
#    struct = eval.get_name_value(struct_name, scope)
#    debug(f"__ref_member__():  struct: {struct}")
#
#    struct_def_name = struct[2]
#    debug(f"__ref_member__():  struct_def_name: {struct_def_name}")
#
#    struct_def_name_value = eval.get_name_value(struct_def_name, scope)
#    debug(f"__ref_member__():  struct_def_name_value: {struct_def_name_value}")
#
#    # get struct member
#    found_member = None
#    found_member_index = None
#    for member_index, member in enumerate(struct_def_name_value[3][0]):
#        debug(f"__ref_member__():  member: {member}")
#        if member[0] == struct_member:
#            found_member = member
#            found_member_index = member_index
#            break
#
#    debug(f"__ref_member__():  found_member: {found_member} found_member_index: {found_member_index}")
#
#    # get struct member type
#    member_type = found_member[1]
#    debug(f"__ref_member__():  member_type: {member_type}")
#
#    converted_member_type = _convert_type(member_type)
#    debug(f"__ref_member__():  converted_member_type: {converted_member_type}")
#
#    stack = []
#
#    debug(f"__ref_member__():  functions_stack: {functions_stack[0]}")
#
#    if not _already_declared(functions_stack[0][0], f"{struct_name}_{struct_member}_member_ptr"):
#        _declare_name(functions_stack[0][0], f"{struct_name}_{struct_member}_member_ptr")
#        stack.append(f"""\t\t%{struct_name}_{struct_member}_member_ptr = getelementptr {member_type}, %{struct_def_name}* %{struct_name}, i64 {member_index}""")
#
#    stack.append(f"""\t\t; get struct "{struct_def_name}" "{struct_name}" member "{struct_member}"
# \t\tload {member_type}, {member_type}* %{struct_name}_{struct_member}_member_ptr
# """)
#
#    retv = "\n".join(stack)
#
#    debug(f"__ref_member__():  retv: {retv}")
#
#    return retv


def __ref_array_member__(node, scope):
    debug(f"__ref_array_member__():  node: {node} scope: {hex(id(scope))}")
    return ['yaya']


def __macro__(node, scope):
    return []


def __if__(node, scope):
    _validate_if(node, scope)

    debug(f"__if__():  backend - node: {node}")

    import eval

    stack = ["\t; if start"]

    # extract elifs from node
    elifs = [child for child in node[3:] if isinstance(child, list) and len(child) > 0 and child[0] == "elif"]
    debug(f"__if__():  backend - elifs: {elifs}")

    # extract else from node
    else_ = [child for child in node[2:] if isinstance(child, list) and len(child) > 0 and child[0] == "else"]
    debug(f"__if__():  backend - !!!! else_: {else_}")

    if len(else_) > 0:
        else_ = else_[0]
    else:
        else_ = None

    debug(f"__if__():  backend - else_: {else_}")

    # get labels
    then_label = _get_var_name("if", "if_then_block_")

    elif_labels = []
    for elif_ in elifs:
        elif_labels.append([elif_, _get_var_name("if", "if_elif_block_")])

    else_label = None
    if else_ is not None:
        else_label = _get_var_name("if", "if_else_block_")

    end_label = _get_var_name("if", "if_end_")

    if len(elif_labels) > 0:
        first_elif = elif_labels[0]
        x_label = first_elif[1] + "_test"

    elif else_ is not None:
        x_label = else_label

    elif else_ is None:
        x_label = end_label

    condition_ir = _get_condition_ir(node, scope, then_label, x_label)
    stack += ["\t\t; if - then condition test"]
    stack += [condition_ir]

    then_code = eval.eval(node[2], scope)
    debug(f"__if__():  backend - then_code: {then_code}")

    stack += [f"\t\t; if - then code block\n\t{then_label}:"] + then_code + [f"\t\tbr label %{end_label}\n"]

    x_size = len(node[3:])
    debug(f"__if__():  backend - x_size: {x_size}")

    for else_index in range(0, 0 + x_size):
        item = node[else_index + 3]
        debug(f"__if__():  backend - else_index: {else_index} item: {item}")

        # handle elif
        if len(item) > 0 and item[0] == "elif":
            debug("__if__():  backend - elif")

            # append elif label
            elif_label = elif_labels[else_index][1]
            stack += [f"\t\t; if - elif condition test\n\t{elif_label}_test:"]

            # check if needs to add branch for next elif
            if len(elif_labels) > else_index + 1:
                next_x_label = elif_labels[else_index + 1][1] + "_test"

            # else, check if there's an else
            elif else_ is not None:
                next_x_label = else_label

            # else, use end_label
            else:
                next_x_label = end_label

            elif_condition_ir = _get_condition_ir(item, scope, elif_label, next_x_label)
            debug(f"__if__():  backend - elif_condition_ir: {elif_condition_ir}")

            stack += [elif_condition_ir]

            # get elif code
            elif_code = eval.eval(item[2], scope)
            debug(f"__if__():  backend - elif_code: {elif_code}")

            stack += [f"\t\t; if - elif code block\n\t{elif_label}:"]
            stack += elif_code
            stack += [f"""\t\tbr label %{end_label}\n"""]

        else:
            debug(f"__if__():  backend - else - item: {item}")

            if len(item) == 0:
                continue

            else_code = eval.eval(item[1], scope)
            debug(f"__if__():  backend - else_code: {else_code}")

            stack += [f"\t\t; if - else code block\n\t{else_label}:"] + else_code + [f"""
\t\tbr label %{end_label}\n"""]

    stack += [f"""\t\t; if - end block
\t{end_label}:
\t; if end
"""]

    debug(f"__if__():  backend - stack: {stack}")

    S = _serialize_body(stack)
    # print(f"S: {S}")

    result = "\n".join(S)
    # result = "\n".join(stack)
    debug(f"__if__():  backend - result: {result}")

    return result


def _validate_if(node, scope):
    pass


def _get_condition_ir(node, scope, then_label, x_label):
    debug(f"_get_condition_ir():  node: {node} scope: {hex(id(scope))} then_label: {then_label} x_label: {x_label}")

    import eval
    condition_name = _get_var_name("if", "condition_")

    if isinstance(node[1], list):
        condition_function = eval.get_name_value(node[1], scope)
        condition_call, stack = return_call(node[1], scope, [])
        debug(f"_get_condition_ir():  backend - condition_call: {condition_call}")

        retv = stack + [f"""\t\t%{condition_name}  = {condition_call}
\t\tbr i1 %{condition_name}, label %{then_label}, label %{x_label}\n"""]
        return "\n".join(retv)

    else:
        # try to infer type
        infered_type_value = eval._infer_type(node[1])
        debug(f"infered_type_value: {infered_type_value}")

        value = None

        # check infered type
        if infered_type_value is not None:
            if infered_type_value[0] != "bool":
                raise Exception("Invalid value for if test: {node}")

            # translate value
            value = "1" if node[1] == "true" else "0"

        else:
            # try to get value
            name_value = eval.get_name_value(node[1], scope)
            debug(f"_get_condition_ir():  backend - name_value: {name_value}")

            if len(name_value) == 0:
                raise Exception(f"Unassigned name: {node[1]}")

            if name_value[2] != "bool":
                raise Exception("Invalid value for if test: {node}")

            value = "1" if name_value[3] == "true" else "0"

        return f"\t\tbr i1 {value}, label %{then_label}, label %{x_label}\n"


def __elif__(node, scope):
    debug(f"__elif__():  backend - node: {node}")


def __data__(node, scope):
    return node[1:]


def __write_ptr__(node, scope):
    return []


def __read_ptr__(node, scope):
    return []


def __get_ptr__(node, scope):
    debug(f"__get_ptr__():  backend - node: {node}")

    import frontend.compiletime as ct
    ct.validate_get_ptr(node, scope)

    get_ptr, name = node

    name_value = eval.get_name_value(name, scope)
    debug(f"__get__ptr__():  backend - name_value: {name_value}")
    type_ = _convert_type(name[2], scope)

    template = f"\t\t%{name}_ptr = getelementptr {type_}, {type_}* %{name}"

    debug(f"__get_ptr__():  backend - template: {template}")

    return template


def __size_of__(node, scope):
    return []


def __unsafe__(node, scope):
    return []


def __linux_open__(node, scope):
    debug(f"__linux_open__():  node: {node}")

    _validate_linux_open(node, scope)

    template = """\t\t%retv = call i64 @linux_open(i8* %filename, i64 %flags, i64 %mode)
\t\tret i64 %retv"""

    return template.split("\n")


def _validate_linux_open(node, scope):
    if len(node) != 4:
        raise Exception(f"Wrong number of arguments for linux_open: {node}")


def __linux_write__(node, scope):
    debug(f"__linux_write__():  node: {node}")

    _validate_linux_write(node, scope)

    # convert fd
    from eval import eval, get_name_value, is_global_name

    # fd = eval([node[1]], scope)
    # debug(f"__linux_write__():  fd: {fd}")
    # fd[0] = _convert_type(fd[0])
    # fd_arg = " ".join(fd)

    name_value = get_name_value(node[1], scope)
    debug(f"__linux_write__():  name_value: {name_value}")

    is_global = is_global_name(name_value, scope)
    debug(f"__linux_write__():  is_global: {is_global}")
    if is_global and name_value[1] == "const":
        converted_type = _convert_type(name_value[2], scope)
        fd_arg = f"{converted_type}* @{node[1]}"

    else:
        debug(f"__linux_write__(): other!")

    text = node[2]

    template = f"""
\t\t%str_addr_ptr = getelementptr i8*, %struct.Str* %text, i64 0
\t\t%str_size_ptr = getelementptr i64, %struct.Str* %text, i64 1

\t\t%fd = load i64, {fd_arg}
\t\t%addr = load i8*, i8** %str_addr_ptr
\t\t%size = load i64, i64* %str_size_ptr

\t\tcall void @linux_write(i64 %fd, i8* %addr, i64 %size)
\t\tret void"""

    return template.split("\n")


def _validate_linux_write(node, scope):
    if len(node) != 3:
        raise Exception(f"Wrong number of arguments for linux_write: {node}")


def __handle__(node, scope):
    debug(f"__handle__():  node: {node} scope: {hex(id(scope))}")

    _validate_handle(node, scope)

    handle_, handled_name, handler_code = node

    handled_name_name_value = eval.get_name_value(handled_name, scope)
    debug(f"__handle__():  handled_name_name_value: {handled_name_name_value}")

    handled_name_type = handled_name_name_value[2]

    handling_generic_variants = False

    # handle generic variants
    variant_concrete_types = None
    variant_type_variables = None
    if isinstance(handled_name_type, list):
        variant_concrete_types = handled_name_type[1:]
        debug(f"__handle__():  backend - variant_concrete_types: {variant_concrete_types}")

        generated_variant_name, converted_type_values = _generate_generic_name(handled_name_type, variant_concrete_types, scope, "generic_variant_")
        handled_name_type = generated_variant_name
        handling_generic_variants = True

        debug(f"__handle__():  backend - generic variant - handled_name_type: {handled_name_type}")

        # get variant type name value
        variant_name_value = eval.get_name_value(handled_name_name_value[2][0], scope)
        debug(f"__handle__():  variant_name_value: {variant_name_value}")

        variant_symbols = [i[0] for i in variant_name_value[3][0][1:]]

        variant_type_variables = variant_name_value[3][0][0][0]
        debug(f"__handle__():  variant_type_variables: {variant_type_variables}")

    # handle not-generic variants
    else:
        # get variant type name value
        variant_name_value = eval.get_name_value(handled_name_name_value[2], scope)
        debug(f"__handle__():  variant_name_value: {variant_name_value}")

        variant_symbols = [t[0] for t in variant_name_value[3][0]]

    debug(f"__handle__():  variant_symbols: {variant_symbols}")

    has_bitcast = len(handled_name_name_value) == 5 and isinstance(handled_name_name_value[4], dict)
    if has_bitcast:
        bitcast_name = handled_name_name_value[4]["bitcast_name"]
        variant_bitcast_type = handled_name_name_value[4]["type"]
        handled_name_type = variant_bitcast_type

    function_name = functions_stack[0][0]
    tmp_handled_name = _get_ir_name(function_name, f"{handled_name}_ptr", last=True)
    # _increment_ir_name_counter(function_name, f"{handled_name}_ptr")

    tmp_tag_value_ptr = _get_ir_name(function_name, f"{handled_name}_tag_ptr")
    _increment_ir_name_counter(function_name, f"{handled_name}_tag_ptr")

    tmp_tag_value = _get_ir_name(function_name, f"{handled_name}_tag_value")
    _increment_ir_name_counter(function_name, f"{handled_name}_tag_value")

    template = [f"""\t\t; handle "{handled_name}"
\t\t; get variant tag
\t\t%{tmp_tag_value_ptr} = getelementptr %{handled_name_type}, %{handled_name_type}* %{tmp_handled_name}, i32 0, i32 0
\t\t%{tmp_tag_value} = load i8, i8* %{tmp_tag_value_ptr}"""]

    tmp_handler_end = _get_ir_name(function_name, f"{handled_name}_handler_end")
    _increment_ir_name_counter(function_name, f"{handled_name}_handler_end")

    # iter over valid tag values
    last_handler_label = None
    enum_variant_symbols = enumerate(variant_symbols)
    for tag_value, tag_symbol in enum_variant_symbols:
        debug(f"__handle__():  tag_value: {tag_value} tag_symbol: {tag_symbol}")

        # generate tag value handler label
        last_tag_value = tag_value == len(variant_symbols) - 1

        if not last_tag_value:
            tmp_tag_value_cmp = _get_ir_name(function_name, f"{handled_name}_tag_value_cmp")
            _increment_ir_name_counter(function_name, f"{handled_name}_tag_value_cmp")

            tmp_tag_type_handler = _get_ir_name(function_name, f"{handled_name}_{tag_symbol}_handler")
            _increment_ir_name_counter(function_name, f"{handled_name}_{tag_symbol}_handler")
        else:
            tmp_tag_type_handler = last_handler_label

        tmp_tag_type_handler_skip = _get_ir_name(function_name, f"{handled_name}_{tag_symbol}_handler_skip")
        _increment_ir_name_counter(function_name, f"{handled_name}_{tag_symbol}_handler_skip")

        # generate llvm ir for compare actual tag value with each valid tag value
        if not last_tag_value:
            template.append(f"""\t\t;
\t\t; compare tag value with {tag_value}
\t\t%{tmp_tag_value_cmp} = icmp eq i8 %{tmp_tag_value}, {tag_value}""")
        else:
            template.append(f"""\t{last_handler_label}:""")

        # generate llvm ir branch
        # check if we're at the penultimate tag value
        penultimate_tag_value = tag_value == len(variant_symbols) - 2

        if last_tag_value:
            pass

        # if penultimate, branch directly to last handler
        elif penultimate_tag_value:
            last_tag_symbol = variant_symbols[-1]
            last_handler_label = _get_ir_name(function_name, f"{handled_name}_{last_tag_symbol}_handler")
            _increment_ir_name_counter(function_name, f"{handled_name}_{last_tag_symbol}_handler")

            template.append(f"""\t\t; branch penultimate
\t\tbr i1 %{tmp_tag_value_cmp}, label %{tmp_tag_type_handler}, label %{last_handler_label}
\t{tmp_tag_type_handler}:""")

        # if not penultimate value, branch to next skip label
        else:
            template.append(f"""\t\t; branch not penultimate
\t\tbr i1 %{tmp_tag_value_cmp}, label %{tmp_tag_type_handler}, label %{tmp_tag_type_handler_skip}
\t{tmp_tag_type_handler}:""")

        _set_branch_ir_name(tmp_tag_type_handler)

        # find out which handler to write IR
        handler_match = None
        if True:  # not handling_generic_variants:
            for each_handler in handler_code:
                debug(f"__handle__():  each_handler: {each_handler}")

                each_handler_tag = each_handler[0]
                debug(f"__handle__():  each_handler_tag: {each_handler_tag}")
                debug(f"__handle__():  tag_symbol: {tag_symbol}")

                if each_handler_tag == tag_symbol:
                    debug(f"__handle__():  handler_match found!")
                    handler_match = each_handler
                    break

        else:
            debug(f"__handle__():  handling generic variant!")

            value_tag_type = handled_name_name_value[3][0]
            debug(f"__handle__():  value_tag_type: {value_tag_type}")

            for each_handler in handler_code:
                debug(f"__handle__():  each_handler: {each_handler}")
                each_handler_tag = each_handler[0]
                if each_handler_tag == value_tag_type:
                    handler_match = each_handler
                    break

        # TODO: transfer to validate
        if handler_match is None:
            raise Exception("handler_match is None!")

        debug(f"__handle__():  handler_match: {handler_match}")

        handler_ir = []

        variant_def = None
        if handling_generic_variants:
            variant_def = variant_name_value[3][0][1:][tag_value]

        for idx, handler_setname in enumerate(handler_match[1:-1]):
            debug(f"__handle__():  idx: {idx} handler_setname: {handler_setname}")

            if not handling_generic_variants:
                handler_name_ir = _get_ir_name(function_name, handler_setname)
                _increment_ir_name_counter(function_name, handler_match[1])

                tmp_value_ptr = _get_ir_name(function_name, f"{handled_name}_value_ptr")
                _increment_ir_name_counter(function_name, f"{handled_name}_value_ptr")

                tmp_value = _get_ir_name(function_name, f"{handled_name}_value")
                _increment_ir_name_counter(function_name, f"{handled_name}_value")

                # find value type
                handler_setname_type = variant_name_value[3][0][tag_value][1 + idx]
                debug(f"__handle__():  handler_setname_type: {handler_setname_type}")

                tmp_value_type = _convert_type(handler_setname_type, scope)

                handler_ir += [[f"""\t\t; get member {idx} value
\t\t%{tmp_value_ptr} = getelementptr %{handled_name_type}, %{handled_name_type}* %{tmp_handled_name}, i32 0, i32 {1+idx}
\t\t%{handler_name_ir} = load {tmp_value_type}, {tmp_value_type}* %{tmp_value_ptr}
"""]]

            else:
                handler_name_ir = _get_ir_name(function_name, f"{handler_setname}")
                _increment_ir_name_counter(function_name, f"{handler_setname}")

                debug(f"__handle__():  variant_def: {variant_def}")

                tag_value_type = variant_def[1:][idx]
                debug(f"__handle__():  tag_value_type: {tag_value_type}")

                # TODO: support nested type variables
                tv_index = None
                for vtv_index, vtv in enumerate(variant_type_variables):
                    debug(f"__handle__():  vtv_index: {vtv_index} vtv: {vtv}")

                    if vtv == tag_value_type:
                        tv_index = vtv_index

                debug(f"__handle__():  tv_index: {tv_index}")

                handler_setname_type = variant_concrete_types[tv_index]
                debug(f"__handle__():  handler_setname_type: {handler_setname_type}")

                tmp_value_type = _convert_type(handler_setname_type, scope)

                tmp_variant_name = handled_name_name_value[0]

                tmp_value_ptr = _get_ir_name(function_name, f"{tmp_variant_name}_value_ptr")
                _increment_ir_name_counter(function_name, f"{tmp_variant_name}_value_ptr")

                tmp_ptr = _get_ir_name(function_name, f"{tmp_variant_name}_ptr", last=True)

                handler_ir += [[f"""\t\t; get pointer of value from "{tmp_variant_name}"
\t\t%{tmp_value_ptr} = getelementptr %{handled_name_type}, %{handled_name_type}* %{tmp_ptr}, i32 0, i32 1
\t\t; loading value for "{handler_setname}"
\t\t%{handler_name_ir} = load {tmp_value_type}, {tmp_value_type}* %{tmp_value_ptr}
"""]]

            # fake up name in scope
            import frontend.compiletime as compiletime
            fake_node = ['def', 'const', handler_setname, [handler_setname_type, "0"]]
            debug(f"__handle__():  fake_node: {fake_node}")
            compiletime.__def__(fake_node, scope)
            debug(f"""__handle__():  scope after def: {scope["names"]}""")

        # generate tag value handler llvm ir
        handler_match_code = handler_match[2]
        debug(f"__handle__():  handler_match_code: {handler_match_code}")

        evaled_handler_match = eval.eval(handler_match[2], scope)
        debug(f"__handle__():  evaled_handler_match: {evaled_handler_match}")

        handler_ir += evaled_handler_match
        debug(f"__handle__():  handler_ir: {handler_ir}")

        _clear_branch_ir_name()

        tmp_handler_ir = []
        for hli in handler_ir:
            tmp_handler_ir.append(hli[0])
        debug(f"__handle__():  tmp_handler_ir: {tmp_handler_ir}")

        serialized_handler_ir = "\n".join(tmp_handler_ir)
        debug(f"__handle__():  serialized_handler_ir: {serialized_handler_ir}")

        template.append(serialized_handler_ir)

        if last_tag_value:
            template.append(f"""\t\t; end of last handler, branch to end
\t\tbr label %{tmp_handler_end}""")

        elif penultimate_tag_value:
            template.append(f"""\t\t; end of penultimate handler, branch to end
\t\tbr label %{tmp_handler_end}""")

        else:
            template.append(f"\t\tbr label %{tmp_handler_end}")
            template.append(f"\t{tmp_tag_type_handler_skip}:")

        if last_tag_value:
            template.append(f"\t{tmp_handler_end}:")

            # add phi nodes
            debug(f"__handle__():  _names_branches: {_names_branches}")

            for name_, branch_data in _names_branches[function_name].items():
                debug(f"__handle__():  name_: {name_}")
                debug(f"__handle__():  branch_data: {branch_data}")

                end_name = _get_ir_name(function_name, name_)
                type_ = "i64"

                phi_node = [f"\t\t%{end_name} = phi {type_} "]

                ct = 0
                for branch_tmp_name, branch_name_ in branch_data:
                    if ct > 0:
                        phi_node.append(", ")

                    phi_node.append(f"[ %{branch_tmp_name}, %{branch_name_} ]")

                    ct += 1

                template.append("".join(phi_node))

    # join it all for retv

    retv = "\n".join(template)
    return retv


def _validate_handle(node, scope):
    debug(f"_validate_handle():  node: {node} scope: {hex(id(scope))}")
    # check min node size
    if len(node) != 3:
        raise Exception(f"Wrong number of arguments for handle: {node}")

    handle_, handled_name, handler_code = node

    handled_name_name_value = eval.get_name_value(handled_name, scope)
    debug(f"_validate_handle():  handled_name_name_value: {handled_name_name_value}")

    # check if name to handle is assigned
    if handled_name_name_value == []:
        raise Exception(f"Unassigned name to handle: {handled_name}")

    handler_code_types = None

    # handle generic variants
    if isinstance(handled_name_name_value[2], list):
        generic_variant_type_main = handled_name_name_value[2][0]
        generic_variant_type_rest = handled_name_name_value[2][1:]

        debug(f"_validate_handle():  generic_variant_type_main: {generic_variant_type_main}")
        debug(f"_validate_handle():  generic_variant_type_rest: {generic_variant_type_rest}")

        handler_code_types = [t[0] for t in handler_code]
        debug(f"_validate_handle():  handler_code_types: {handler_code_types}")

        variant_types = handler_code_types

    # handle not-generic variants
    else:
        handler_code_types = [t[0] for t in handler_code]
        debug(f"_validate_handle():  handler_code_types: {handler_code_types}")

        # get variant type name value
        variant_name_value = eval.get_name_value(handled_name_name_value[2], scope)
        debug(f"_validate_handle():  variant_name_value: {variant_name_value}")

        # check if all variant types are handled
        variant_types = [t[0] for t in variant_name_value[3][0]]

    debug(f"_validate_handle():  variant_types: {variant_types}")

    missing_handlers = [t for t in variant_types if t not in handler_code_types]
    if len(missing_handlers) > 0:
        raise Exception(f"""Missing handlers for variant "{handled_name}": {",".join(missing_handlers)}""")

    wrong_handlers = [t for t in handler_code_types if t not in variant_types]
    if len(wrong_handlers) > 0:
        raise Exception(f"""Wrong handlers for variant "{handled_name}": {",".join(wrong_handlers)}""")


def __use__(node, scope):
    debug(f"__use__():  backend - node: {node}")
    return []


def return_call(node, scope, stack=[]):
    debug(f"return_call():  node: {node} scope: {hex(id(scope))} stack: {stack}")

    li_name = node[0]

    value = eval.get_name_value(li_name, scope)
    debug(f"return_call():  value: {value}")

    if value == []:
        raise Exception(f"No name matches for name: {li_name}")

    if value[2] == "fn":
        debug(f"return_call():  calling a function")

        method, solved_arguments = eval.find_function_method(node, value, scope)
        debug(f"return_call():  method: {method}")

        function_arguments = method[0][0]
        debug(f"return_call():  function_arguments: {function_arguments}")

        function_name = _unoverload(li_name, function_arguments)
        debug(f"return_call():  function_name: {function_name}")

        # find out arguments
        converted_arguments = []
        for argument_index, argument in enumerate(function_arguments):
            debug(f"return_call():  argument: {argument}")

            if len(argument) == 2:
                name, type_ = argument
            elif len(argument) == 3 and argument[1] == "mut":
                name, mutdecl, type_ = argument

            converted_type = _convert_type(type_, scope)

            # fulfill llvm requirement to convert float values types to doubles
            if converted_type == "float":
                converted_type = "double"

            argument_value = node[argument_index + 1]
            debug(f"return_call():  argument_value: {argument_value}")

            if isinstance(argument_value, list):
                debug(f"return_call():  argument_value is list - argument_value: {argument_value}")

                scope_argument_value = eval.get_name_value(argument_value[0], scope)

                debug(f"return_call():  before return_call() call - stack: {stack}\n")

                result, stack = return_call(argument_value, scope, stack.copy())

                debug(f"return_call():  after return_call() call - result: {result} stack_: {stack}")

                var_name_function = functions_stack[0][0]

                tmp = _get_var_name(var_name_function, "tmp_")
                call_ = f"\t\t%{tmp} = {result}"

                debug(f"return_call():  appending call_ {call_} to stack")

                stack.append(call_)

                debug(f"return_call():  stack after append: {stack}")

                argument_value = f"%{tmp}"

            else:
                debug(f"return_call():  argument_value isn't list - argument_value: {argument_value}")

                scope_argument_value = eval.get_name_value(argument_value, scope)
                debug(f"return_call():  scope_argument_value: {scope_argument_value}")

                if scope_argument_value != []:
                    argument_name = False
                    if len(functions_stack) > 0:
                        debug(f"return_call():  len(functions_stack) > 0")

                        arguments_matches = [arg for arg in functions_stack[len(functions_stack) - 1][2] if argument_value == arg[0]]
                        # print(f"arguments_matches: {arguments_matches}")

                        if len(arguments_matches) > 0:
                            argument_value = f"%{argument_value}"
                            argument_name = True

                    if not argument_name:
                        debug(f"return_call():  not argument_name")

                        # check for generic types
                        if isinstance(scope_argument_value[2], list):
                            debug(f"return_call():  scope_arguent_value[2] is list")

                            if scope_argument_value[2][0] == "Array":
                                argument_value = f"%{argument_value}"

                        # not generic types
                        else:
                            debug(f"return_call():  scope_arguent_value[2] isn't list")

                            if scope_argument_value[1] == 'const':
                                debug(f"return_call():  scope_argument_value is const - argument_value: {argument_value}")

                                if scope_argument_value[2] in ["int", "uint", "byte"]:
                                    debug(f"return_call():  scope_argument_value is int")
                                    argument_value = f"%{argument_value}"

                                else:
                                    debug(f"return_call():  scope_argument_value isn't int")
                                    cur_function_name = functions_stack[0][0]  # "main_"
                                    argument_value = f"@{cur_function_name}{argument_value}"
                                    converted_type += "*"

                            elif scope_argument_value[1] == 'mut':
                                debug(f"return_call():  scope_argument_value is mut")
                                ir_name = _get_ir_name(function_name, argument_value)
                                argument_value = f"%{ir_name}"

                    # print(f"argument_value: {argument_value}")

            converted_argument = f"{converted_type} {argument_value}"

            debug(f"return_call():  converted_argument: {converted_argument}")

            converted_arguments.append(converted_argument)

        converted_function_arguments = ", ".join(converted_arguments)

        # find out return type
        if len(method) == 2:
            function_return_type = "void"

        elif len(method) == 3:
            function_return_type = _convert_type(method[1], scope)

        ret_value = f"call {function_return_type} @{function_name}({converted_function_arguments})"

    elif value[2] == "internal":
        debug(f"return_call():  calling an internal")

        ret_value = value[3](node, scope)

    debug(f"return_call():  exiting return_call():  value: {value} ret_value: {ret_value} stack: {stack}\n")

    return ret_value, stack


_var_names = {}


def _get_var_name(function_name, prefix):
    debug(f"_get_var_name():  function_name: {function_name} prefix: {prefix}")

    # initialize key in _var_names for function_name
    if function_name not in _var_names.keys():
        _var_names[function_name] = 0

    # get name
    var_name = f"{prefix}{_var_names[function_name]}"

    # increment counter
    _var_names[function_name] += 1

    debug(f"_get_var_name():  var_name: {var_name}")

    return var_name


_names_storage = {}
_names_branches = {}

_branch_name = None
_previous_branch_names = []


def _get_ir_name(function_name, name, last=False, track_branching=False):
    debug(f"_get_ir_name():  function_name: {function_name} name: {name}")

    # initialize key in _names_storage for function_name
    if function_name not in _names_storage.keys():
        _names_storage[function_name] = {}

    # initialize key in _names_storage[function_name] for name
    if name not in _names_storage[function_name].keys():
        _names_storage[function_name][name] = 0

    if last:
        ir_name = f"{name}_{_names_storage[function_name][name] - 1}"
    else:
        ir_name = f"{name}_{_names_storage[function_name][name]}"

    debug(f"_get_ir_name():  ir_name: {name}")
    debug(f"_get_ir_name():  _branch_name: {_branch_name}")

    if _branch_name is not None and track_branching:
        if function_name not in _names_branches.keys():
            debug(f"_get_ir_name():  setting _names_branches[{function_name}]")
            _names_branches[function_name] = {}

        if name not in _names_branches[function_name].keys():
            debug(f"_get_ir_name():  setting _names_branches[{function_name}][{name}]")
            _names_branches[function_name][name] = []

        # _names_branches[function_name][name].append([ir_name, _branch_name])

        _names_branches[function_name][name].append([ir_name, _branch_name])

        debug(f"_get_ir_name(): _names_branches: {_names_branches}")

    return ir_name  # _names_storage[function_name][name]


def _increment_ir_name_counter(function_name, name):
    debug(f"_increment_ir_name_counter():  function_name: {function_name} name: {name}")
    _names_storage[function_name][name] += 1


def _set_branch_ir_name(branch_name):
    debug(f"_set_branch_ir_name():  branch_name: {branch_name}")

    global _branch_name
    global _previous_branch_names

    debug(f"_set_branch_ir_name():  before _previous_branch_names: {_previous_branch_names}")
    debug(f"_set_branch_ir_name():  before _branch_name: {_branch_name}")

    _previous_branch_names.append(_branch_name)
    _branch_name = branch_name

    debug(f"_set_branch_ir_name():  after _previous_branch_names: {_previous_branch_names}")
    debug(f"_set_branch_ir_name():  after _branch_name: {_branch_name}")


def _clear_branch_ir_name():
    debug(f"_clear_branch_ir_name()")
    global _branch_name

    debug(f"_clear_branch_ir_name():  before _previous_branch_names: {_previous_branch_names}")
    debug(f"_clear_branch_ir_name():  before _branch_name: {_branch_name}")

    _branch_name = _previous_branch_names.pop()

    debug(f"_clear_branch_ir_name():  after _previous_branch_names: {_previous_branch_names}")
    debug(f"_clear_branch_ir_name():  after _branch_name: {_branch_name}")


_declared_names = {}


def _declare_name(function_name, name):
    if function_name not in _declared_names.keys():
        _declared_names[function_name] = []

    _declared_names[function_name].append(name)


def _already_declared(function_name, name):
    return function_name in _declared_names.keys() and name in _declared_names[function_name]


_defined_variants = []


def _define_variant(variant):
    global _defined_variants
    _defined_variants.append(variant)


def _variant_already_defined(variant):
    global _defined_variants
    return variant in _defined_variants


# SCOPE HOOKS
#
def return_call_common_list(evaled, name_match, scope):
    debug(f"return_call_common_list():  evaled: {evaled} name_match: {name_match}")

    # check if is returning a name storing a struct member value
    if name_match[3][0] == "ref_member" or name_match[3][0] == "ref_array_member":
        name_match_type = name_match[2]
        type_return_call = _convert_type(name_match_type, scope)
        name_return_call = f"%{name_match[0]}"

    else:
        evaled_fn = eval.get_name_value(evaled[0], scope)
        method, solved_arguments = eval.find_function_method(evaled, evaled_fn, scope)
        method_type = method[1]

        if name_match[2] != method_type:
            raise Exception(f"Name and evaluated value types are different - name_match type: {name_match[2]} - method_type: {method_type}")

        debug(f"_return_call_common():  new method_type: {method_type}")

        # TODO: get correct return type and result name

        # get type
        type_return_call = "i64"

        # get var name
        name_return_call = "%result"

    li = [f"""\t\t; return_call_common_list
\t\tret {type_return_call} {name_return_call}"""]

    return li


def return_call_common_not_list(name_match, scope):
    debug(f"""return_call_common_not_list():  backend scope - name_match: {name_match} function_depth: {scope["function_depth"]} - is_last: {scope["is_last"]}""")

    if scope["is_last"] and scope["function_depth"] == 1:

        seek_name_match = ""
        # handle composite types
        if isinstance(name_match[2], list):
            # get only first name of composite types
            seek_name_match = name_match[2][0]
        # handle simple types
        else:
            seek_name_match = name_match[2]

        debug(f"""return_call_common_not_list():  backend scope - seek_name_match: {seek_name_match}""")

        name_match_name_value = eval.get_name_value(seek_name_match, scope)
        debug(f"""return_call_common_not_list():  backend scope - name_match_name_value: {name_match_name_value}""")

        if name_match_name_value != []:
            if name_match_name_value[2] == "variant":
                debug(f"""return_call_common_not_list():  backend scope - handling variant""")

                variant_types = name_match_name_value[3][0]
                debug(f"""return_call_common_not_list():  backend scope - variant_types: {variant_types}""")

                target = variant_types[0][0]
                debug(f"""return_call_common_not_list():  backend scope - target: {target}""")

                is_generic = isinstance(target, list)
                debug(f"""return_call_common_not_list():  backend scope - is_generic: {is_generic}""")

                if not is_generic:
                    converted_type = f"%{name_match[2]}"
                else:
                    joined_names = "_".join(name_match[2])
                    converted_type = f"%generic_variant_{joined_names}"
            else:
                debug(f"""return_call_common_not_list():  backend scope - handling not variant""")
                converted_type = _convert_type(name_match[2], scope)

        debug(f"""return_call_common_not_list():  backend scope - converted_type: {converted_type}""")

        # check if it's a mut name to get last declared IR name
        if name_match[1] == "mut":
            debug(f"""return_call_common_not_list():  backend scope - handling mut name""")
            tmp_name = _get_ir_name(functions_stack[0][0], name_match[0])
            li = [f"\t\tret {converted_type} %{tmp_name}"]

        else:
            debug(f"""return_call_common_not_list():  backend scope - handling const name""")

            is_variant = False

            if name_match_name_value != [] and name_match_name_value[2] == "variant":
                debug(f"""return_call_common_not_list():  backend scope - handling const variant""")

                name_match_ = _get_ir_name(functions_stack[0][0], name_match[0])
                li = [f"\t\tret {converted_type} %{name_match_}"]

                is_variant = True

            if not is_variant:
                li = [f"\t\tret {converted_type} %{name_match[0]}"]

    else:
        li = ["\t\t; NOT IMPLEMENTED"]

    debug(f"""return_call_common_not_list():  backend scope - return li: {li}""")

    return ["\t\t; return_call_common_not_list"] + li


def return_variant(name_match, scope):
    debug(f"return_variant():  backend scope - name_match: {name_match}")

    type_ = None
    if not isinstance(name_match[2], list):
        type_ = f"%{name_match[2]}"
    else:
        type_ = name_match[2]
        type_values = ['int', 'bool']
        generated_variant_name, converted_type_values = _generate_generic_name(type_, type_values, scope, "generic_variant_")

        # CAUTION: it should not have the tag name because it's returned and function return types must keep the untagged generic variant type
        type_ = f"%{generated_variant_name}"

    name = _get_ir_name(functions_stack[0][0], name_match[0])
    bitcast_name = name_match[4]["bitcast_name"]

    retv = f"""\t\t; return_variant
\t\t%{name} = load {type_}, {type_}* %{bitcast_name}
\t\tret {type_} %{name}"""

    debug(f"return_variant():  backend scope - retv: {retv}")
    return retv

#
#


scope = copy.deepcopy(eval.default_scope)


def _setup_scope():
    from . import bool as bool_
    from . import uint as uint
    from . import int as int_
    from . import float as float_
    from . import byte as byte

    names = [
    ["fn", "mut", "internal", __fn__],
    ["def", "mut", "internal", __def__],
    ["set", "mut", "internal", __set__],
    ["set_array_member", "mut", "internal", __set_array_member__],
    ["get_array_member", "mut", "internal", __get_array_member__],
    ["set_struct_member", "mut", "internal", __set_struct_member__],
    ["get_struct_member", "mut", "internal", __get_struct_member__],
    ["ref_member", "mut", "internal", __ref_member__],
    ["ref_array_member", "mut", "internal", __ref_array_member__],
    ["macro", "mut", "internal", __macro__],

    ["if", "mut", "internal", __if__],
    ["elif", "mut", "internal", __elif__],

    ["data", "mut", "internal", __data__],
    ["write_ptr", "mut", "internal", __write_ptr__],
    ["read_ptr", "mut", "internal", __read_ptr__],
    ["get_ptr", "mut", "internal", __get_ptr__],
    ["size_of", "mut", "internal", __size_of__],
    ["unsafe", "mut", "internal", __unsafe__],

    ["handle", "mut", "internal", __handle__],

    ["use", "mut", "internal", __use__],

    ["linux_open", "const", "internal", __linux_open__],
    ["linux_write", "const", "internal", __linux_write__],

    ["int", "const", "type", [8]],

    ["uint", "const", "type", [8]],

    ["ptr", "const", "type", [8]],

    ["byte", "const", "type", [1]],
    ["bool", "const", "type", [1]],

    ["float", "const", "type", [8]],

    ["Array", "const", "type", ['?']],
    ["struct", "const", "type", ['?']],
    ["enum", "const", "type", ['?']],
    ["variant", "const", "type", ['?']],

    ["Str", "const", "type", ['?']],

    ["and_bool_bool", "const", "internal", bool_.__and_bool_bool__],
    ["or_bool_bool", "const", "internal", bool_.__or_bool_bool__],
    ["xor_bool_bool", "const", "internal", bool_.__xor_bool_bool__],
    ["not_bool", "const", "internal", bool_.__not_bool__],
    ["eq_bool_bool", "const", "internal", bool_.__eq_bool_bool__],

    ["add_int_int", "const", "internal", int_.__add_int_int__],
    ["sub_int_int", "const", "internal", int_.__sub_int_int__],
    ["mul_int_int", "const", "internal", int_.__mul_int_int__],
    ["div_int_int", "const", "internal", int_.__div_int_int__],
    ["and_int_int", "const", "internal", int_.__and_int_int__],
    ["or_int_int", "const", "internal", int_.__or_int_int__],
    ["xor_int_int", "const", "internal", int_.__xor_int_int__],
    ["not_int", "const", "internal", int_.__not_int__],
    ["eq_int_int", "const", "internal", int_.__eq_int_int__],
    ["gt_int_int", "const", "internal", int_.__gt_int_int__],
    ["ge_int_int", "const", "internal", int_.__ge_int_int__],
    ["lt_int_int", "const", "internal", int_.__lt_int_int__],
    ["le_int_int", "const", "internal", int_.__le_int_int__],
    ["shl_int_int", "const", "internal", int_.__shl_int_int__],
    ["shr_int_int", "const", "internal", int_.__shr_int_int__],

    ["add_uint_uint", "const", "internal", uint.__add_uint_uint__],
    ["sub_uint_uint", "const", "internal", uint.__sub_uint_uint__],
    ["mul_uint_uint", "const", "internal", uint.__mul_uint_uint__],
    ["div_uint_uint", "const", "internal", uint.__div_uint_uint__],
    ["and_uint_uint", "const", "internal", uint.__and_uint_uint__],
    ["or_uint_uint", "const", "internal", uint.__or_uint_uint__],
    ["xor_uint_uint", "const", "internal", uint.__xor_uint_uint__],
    ["not_uint", "const", "internal", uint.__not_uint__],
    ["eq_uint_uint", "const", "internal", uint.__eq_uint_uint__],
    ["gt_uint_uint", "const", "internal", uint.__gt_uint_uint__],
    ["ge_uint_uint", "const", "internal", uint.__ge_uint_uint__],
    ["lt_uint_uint", "const", "internal", uint.__lt_uint_uint__],
    ["le_uint_uint", "const", "internal", uint.__le_uint_uint__],
    ["shl_uint_int", "const", "internal", uint.__shl_uint_int__],
    ["shr_uint_int", "const", "internal", uint.__shr_uint_int__],

    ["add_byte_byte", "const", "internal", byte.__add_byte_byte__],

    ["add_float_float", "const", "internal", float_.__add_float_float__],
    ["sub_float_float", "const", "internal", float_.__sub_float_float__],
    ["mul_float_float", "const", "internal", float_.__mul_float_float__],
    ["div_float_float", "const", "internal", float_.__div_float_float__],
    ["eq_float_float", "const", "internal", float_.__eq_float_float__],
    ["gt_float_float", "const", "internal", float_.__gt_float_float__],
    ["ge_float_float", "const", "internal", float_.__ge_float_float__],
    ["lt_float_float", "const", "internal", float_.__lt_float_float__],
    ["le_float_float", "const", "internal", float_.__le_float_float__],

    ]

    for name in names:
        scope["names"].append(name)

    scope["return_call"] = return_call
    scope["return_call_common_list"] = return_call_common_list
    scope["return_call_common_not_list"] = return_call_common_not_list
    scope["return_variant"] = return_variant
    scope["step"] = "backend"


_setup_scope()
